#########################################################################
############################Inscription##################################
#########################################################################

#Inscrit le user

INSERT INTO Membre(pseudo,password,picture,birthday,ville,mail,entreprise,score)
VALUES (:pseudo,:pass,:picture,:birthday,:ville,:mail,:entreprise,0)


#Check si pseudo déjà utilisé

SELECT pseudo
FROM Membre
WHERE pseudo=:pseudo


#Recupere toutes les villes deja inscrites

SELECT ville
FROM Membre



#########################################################################
#############################Connexion###################################
#########################################################################


#Check si identifiant et mot de pass ok

SELECT idmembre
FROM Membre
WHERE pseudo = :pseudo AND password = :password



#########################################################################
###############################Profil####################################
#########################################################################

#Recupere la liste des noms des groupes d'un user

SELECT nom_groupe
FROM Groupe
WHERE idgroupe IN (
	SELECT idgroupe
	FROM Jonction
	WHERE idmembre=:idmembre)


#Recupere la liste des noms des paris d'un user

SELECT nom_paris
FROM Paris
WHERE idparis IN (
	SELECT idparis
	FROM Jonction
	WHERE idmembre=:idmembre)

#Recupere la liste des noms des paris d'un user par groupe

SELECT nom_paris
FROM Paris
WHERE idparis IN (
	SELECT idparis
	FROM Jonction
	WHERE idmembre=:idmembre AND idgroupe=:idgroupe)


#Recupere le score d'un user dans un groupe

SELECT score_groupe
FROM Jonction
WHERE idmembre=:idmembre AND idgroupe=:idgroupe


#Recupere la photo de profil d'un user

SELECT picture
FROM Membre
WHERE idmembre=:idmembre


#Recupere le pseudo d'un user

SELECT pseudo
FROM Membre
WHERE idmembre=:idmembre



#########################################################################
##############################Main Page##################################
#########################################################################

#Recupere la somme des score

SELECT SUM(score) AS score_tot
FROM Membre
WHERE idmembre=:idmembre 

#Barre de recherche ~ users 
SELECT pseudo
FROM Membre
WHERE LOWER(pseudo)=LOWER(:search)

#Barre de recherche ~ Paris
SELECT nom_paris
FROM Paris
WHERE LOWER(nom_paris)=LOWER(:search)

#Barre de recherche ~ Groupe
SELECT nom_groupe
FROM Groupe
WHERE LOWER(nom_groupe)=LOWER(:search)

#Recupérer la date de création de pari en fonction d'une date donnée -> idem pour Groupe, juste changer la table dans le FROM
SELECT idparis, nom_paris
FROM Paris
WHERE creation_date=:creation_date


#########################################################################
################################Paris####################################
#########################################################################

#Récupère le vote d'un membre
SELECT texte
FROM Vote
WHERE idmembre=:idmembre AND idparis=:idparis

#Entre l'identifiant correspondant à la réponse
UPDATE Vote
SET idreponse_vote=:value
WHERE idmembre=:idmembre AND idparis=:idparis

#Entre l'identifiant de la bonne réponse dans le paris
UPDATE Paris
SET idreponse_paris=:value
WHERE idparis=:idparis


#########################################################################
##########################Créations diverses#############################
#########################################################################

#Créer un groupe
INSERT INTO Groupe(nom_groupe,creation_date_groupe,idadmin_groupe,description_groupe)
VALUES (:nom_groupe,:creation_date_groupe,$_SESSION['idmembre'],:description_groupe)

#Créer un paris
INSERT INTO Paris(nom_paris,idamin_paris,creation_date,delete_date,ville,entreprise,description,idreponse_paris)
VALUES (:nom_paris,$_SESSION['idmembre'],:creation_date,:delete_date,:ville,:entreprise,:description,0)	#0 correspond à l'idreponse par défaut

#Poste un vote
INSERT INTO Vote(idparis,idmembre,texte,idreponse_vote)
VALUES (:idparis,$_SESSION['idmembre'],:texte,:idreponse_vote)