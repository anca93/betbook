<?php

  //IL FAUT MODIFIER LA CONNECTION À LA BASE DE DONNÉE
session_start();
  //connection à la BDD
try
{
  $bdd = new PDO('mysql:host=localhost;dbname=BetBook;charset=utf8', 'root', '');
}
catch(Exception $e)
{
  die('Erreur : '.$e->getMessage());
} 
    $_SESSION['idmembre']=1;                //à supprimer !
    //variable à initialiser avec les variable de session
    $idMembre = $_SESSION['idmembre'];
    $nbrParis = 0;

    //selectionne les Identifiants des paris du l'utilisteur connecté
    //**********************************************************************************//
    $nbrBets= $bdd->prepare('SELECT DISTINCT idparis FROM Jonction WHERE idmembre=:idmembre');
    $nbrBets->bindParam(':idmembre', $idmembre, PDO::PARAM_INT);
    //**********************************************************************************//

    //selectionne les différents nom des groupes de l'utilisateur connecté
    //**********************************************************************************//
    $header = $bdd->prepare('SELECT DISTINCT nom_groupe FROM Groupe,Jonction WHERE Link.idmembre = :idmembre AND Groupe.idmembre = Link.idgroupe');
    $header->bindParam(':idmembre', $idmembre, PDO::PARAM_INT);
    //**********************************************************************************//

    //selection les paris d'un groupe d'un utilisateur
    //**********************************************************************************//
    $bets = $bdd->prepare('SELECT DISTINCT nom_paris FROM Paris, Jonction, Groupe WHERE Link.idparis=Paris.idparis AND Link.idgroupe = Groupe.idgroupe AND link.idmembre = ?  AND Groupe.nom_groupe = ?' );
    //**********************************************************************************//

    



    //Recupère les pseudo des joueurs en fonction de leur score
    $recuppseudo = $bdd->prepare('SELECT idmembre, pseudo FROM Membre ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur entreprise
    $recuppseudoentreprise = $bdd->prepare('SELECT idmembre, pseudo FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur ville
    $recuppseudoville = $bdd->prepare('SELECT idmembre, pseudo FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur groupe
    $recuppseudogroup = $bdd->prepare('SELECT Membre.idmembre, pseudo FROM Membre, Jonction WHERE idgroupe=:idgroupe AND Jonction.idmembre=Membre.idmembre ORDER BY score_groupe DESC LIMIT 30');
    //**********************************************************************************//






    //Récupère le score
    $recupscore = $bdd->prepare('SELECT idmembre, score FROM Membre ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//


    //Récupère le score par entreprise
    $recupscoreentreprise = $bdd->prepare('SELECT idmembre, score FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Récupère le score par entreprise
    $recupscoreville = $bdd->prepare('SELECT idmembre, score FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Récupère le score par entreprise
    $recupscoregroup = $bdd -> prepare ('SELECT idmembre, score_groupe FROM Jonction WHERE idgroupe=:idgroupe ORDER BY score_groupe DESC LIMIT 30');
    //**********************************************************************************//





    //Recupère le nb max de personnes, sans dépasser 30
    $recupclass = $bdd->prepare('SELECT COUNT(*) AS class FROM Membre  LIMIT 30');
    //**********************************************************************************//

    //Recupère le nb max de personnes par entreprise, sans dépasser 30
    $recupclassentreprise = $bdd->prepare('SELECT COUNT(*) AS class FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) LIMIT 30');
    //**********************************************************************************//

    //Recupère le nb max de personnes par ville, sans dépasser 30
    $recupclassville = $bdd->prepare('SELECT COUNT(*) AS class FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) LIMIT 30');
    //**********************************************************************************//

    //Recupère le nb max de personnes dans un groupe
    $recupclassgroup = $bdd->prepare('SELECT COUNT(*) AS class FROM Jonction WHERE idgroupe=:idgroupe LIMIT 30');
    //**********************************************************************************//





    //Recupère l'id des membres', sans dépasser 30
    $recupid = $bdd->prepare('SELECT idmembre FROM Membre ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère l'id des membres dans un groupe, sans dépasser 30
    $recupidgroup = $bdd->prepare('SELECT idmembre FROM Jonction WHERE idgroupe=:idgroupe ORDER BY score_groupe DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère l'id des membres par entreprise, sans dépasser 30
    $recupidentreprise = $bdd->prepare('SELECT idmembre FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur ville
    $recupidville = $bdd->prepare('SELECT idmembre FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //récupère le nom des groupes d'un user
    $recupgroup = $bdd->prepare('SELECT nom_groupe, idgroupe FROM Groupe WHERE idgroupe IN (SELECT idgroupe FROM Jonction WHERE idmembre=:idmembre)');
    //**********************************************************************************//




    //Recupère le pseudo du user connecté
    $recuppseudouser = $bdd->prepare('SELECT pseudo FROM Membre WHERE idmembre=:idmembre');
    //**********************************************************************************//

    //Recupère le score du user connecté
    $recupscoreuser = $bdd->prepare('SELECT score FROM Membre WHERE idmembre=:idmembre');
    //**********************************************************************************//

    //Recupère le classement du user connecté
    //$recupclassuser = $bdd->prepare('SELECT score FROM Membre WHERE idmembre=:idmembre');
    //**********************************************************************************//

    //Recupère le score du user connecté dans un groupe
    $recupscoregroupeuser = $bdd->prepare('SELECT score FROM Jonction WHERE idmembre=:idmembre AND idgroupe=:idgroupe');
    //**********************************************************************************//



    //calcule le nombre de paris de l'utilisateur connecté
    //**********************************************************************************//
    $nbrBets->execute();
    $nbrParis = 0;
    while($donnees = $nbrBets->fetch())
    {
      $nbrParis += 1;
    }
    $nbrBets->closeCursor();    
    //**********************************************************************************//
    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>

      <!-- Required meta tags always come first -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <meta http-equiv="x-ua-compatible" content="ie=edge">


      <!-- Bootstap 4 -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">

      <!-- CSS perso -->
      <link rel="stylesheet" href="bootstrap/css/main.css">

      <!-- pour les icons -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    </head>
    <body>
      <div class="background_opacity">

        <div class="container">
          <div class="row">
            <div class="col-md-3">

              <div class="bootstrap-vertical-nav">

                <nav class="navbar navbar-light bg-faded">
                  <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
                    &#9776;
                  </button>
                  <div class="collapse navbar-toggleable-sm" id="exCollapsingNavbar2">

                    <a class="navbar-brand" href="#">Welcome to BetBook</a>

                    <ul class="nav navbar-nav">

                     <li class="nav-item active">
                      <a class="nav-link" href="#">                                                 <i class="fa fa-user fa-fw bg2 ellipsis"></i>
                        Profile
                      </a>
                    </li>   

                    <li class="nav-item active">
                      <a class="nav-link  ellipsis titleGroups" href="#">                                                 <i class="fa fa-users fa-fw bg2 ellipsis"></i>
                        My groups
                      </a>
                    </li>

                    <?php
                    $header->execute();
                    while($donneesHeader = $header->fetch())
                    {
                      ?>

                      <li class="nav-item active">
                        <a class="nav-link text-primary ellipsis  " href="#">#<?php echo $donneesHeader['nom_groupe']?><span class="sr-only">(current)</span></a>
                      </li>
                      <?php
                    }
                    ?>

                    <li class="nav-item active">
                      <div class="dropdown">
                        <button class="btn btn-primary btn-block dropdown-toggle" type="button" data-toggle="dropdown">My bets
                          <span class="bagde"> (<?php echo $nbrParis?>) </span></button>
                          <ul class="dropdown-menu">

                            <?php
                            $header->execute();
                            while($donneesHeader = $header->fetch())
                            {
                              ?>
                              <li class="dropdown-header"><?php echo $donneesHeader['nom_groupe']?></li>
                              <?php
                              $bets->execute(array($idmembre, $donneesHeader['nom_groupe']));
                              while($donneesBets = $bets->fetch())
                              {
                                ?>
                                <li class="dropdown_menu">
                                  <a href="#">
                                    <i> <?php echo $donneesBets['nom_paris']?></i>
                                  </a>
                                </li>
                                <?php
                              }
                              $bets->closeCursor();
                            }
                            $header->closeCursor();
                            ?>
                          </ul>
                        </div>
                      </li>

                      <li class="nav-item active">
                        <a class="nav-link ellipsis" href="#">                                                <i class="fa fa-cogs fa-fw bg2 ellipsis"></i>
                          Settings
                        </a>
                      </li>

                    </ul>
                  </div>
                </nav>
              </div>
            </div>
            <div class="col-md-9 top">
              <div class="row">
                <form class="navbar-form navbar-left" role="search">
                  <div class="col-md-9 col-xs-8">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search BetBook">
                    </div>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    <form method="post" action="#">
                      <button type="submit" class="btn btn-primary btn-block shadow"><i class="fa fa-eye fa-1x" aria-hidden="true"></i> Submit</button>
                    </form>
                  </div>
                </form>

                <div class="col-md-12 col-xs-12">
                  <h1 class="row">
                    Ranking
                  </h1>
                </div>
                
                <div class="col-md-12 col-xs-12">
                  <div class="col-md-3 col-xs-3 subpolice">
                    <a href="classement.php?ranking=1"><span style="display:block; border:solid 1px grey;">World</span></a></br>
                  </div>

                  <div class="col-md-3 col-xs-3 subpolice_group dropdown">
                    <span style="display:block; border:solid 1px grey;">Group ▿<div class="col-md-12 col-xs-12 dropdown-content">
                      <?php 
                      $recupgroup->execute(array('idmembre' => $_SESSION['idmembre']));

                      while ($donnees = $recupgroup->fetch()){
                        ?><font size="4"><a href="classement.php?ranking=2&idgroup=<?php echo $donnees['idgroupe']?>"><?php echo $donnees['nom_groupe']; echo " "; ?></a></font><?php
                      }
                      ?>

                    </div></span>

                  </br>

                </div>
                <div class="col-md-3 col-xs-3 subpolice">
                  <a href="classement.php?ranking=3"><span style="display:block; border:solid 1px grey;">Entreprise</span></a></br>
                </div>
                <div class="col-md-3 col-xs-3 subpolice">
                  <a href="classement.php?ranking=4"><span style="display:block; border:solid 1px grey;">City</span></a></br>
                </div>
              </div>

            <!--  <div class="col-md-12 col-xs-12">
                <div class="col-md-4 col-xs-4  subpolice">
                  <?php
                    $recuppseudouser->execute(array('idmembre' => $_SESSION['idmembre']));
                    $donnees = $recuppseudouser->fetch();
                    echo $donnees['pseudo'];
                  ?>
                </div>

                <div class="col-md-4 col-xs-4 subpolice">
                  <?php
                //    $recupclassuser
                  ?>
                </div>
                <div class="col-md-4 col-xs-4 subpolice">
                  <?php
                    $recupscoreuser->execute(array('idmembre' => $_SESSION['idmembre']));
                    $donnees = $recupscoreuser->fetch();
                    echo $donnees['score'];
                  ?>
                </div>
              </div>   -->           


              <div class="col-md-12 col-xs-12 backstrip">
                <div class="col-md-4 col-xs-4  subpolice">
                  Rank

                </div>

                <div class="col-md-4 col-xs-4 subpolice">
                  Pseudo
                </div>
                <div class="col-md-4 col-xs-4 subpolice">
                  Points
                </div>
              </div>

              <div class="col-md-12 col-xs-12 backrank">
                <div class="col-md-4 col-xs-4 subrank">
                  <?php 
                  if (isset($_GET['ranking']) AND $_GET['ranking']==1){
                    $recupclass->execute();
                    $recupid->execute();

                    $donnees = $recupclass->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupid->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }else{
                        echo $i;
                        echo "<br>";
                      }
                      
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==2){

                    if (isset($_GET['idgroup'])){

                      $recupclassgroup->execute(array('idgroupe' => $_GET['idgroup']));
                      $recupidgroup->execute(array('idgroupe' => $_GET['idgroup']));

                      $donnees = $recupclassgroup->fetch();
                      $nb=$donnees['class'];
                      $nb=intval($nb);

                      for ($i = 1; $i <= $nb; $i++) {
                        $donnees2 = $recupidgroup->fetch();
                        if($donnees2['idmembre'] == $_SESSION['idmembre']){
                          ?><b><font size="4"><?php echo $i; ?></font></b><?php
                          echo "<br>";
                        }else{
                          echo $i;
                          echo "<br>";
                        }

                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==3){
                    $recupclassentreprise->execute(array('idmembre' => $_SESSION['idmembre']));
                    $recupidentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    $donnees = $recupclassentreprise->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupidentreprise->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }else{
                        echo $i;
                        echo "<br>";
                      }
                      
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==4){
                    $recupclassville->execute(array('idmembre' => $_SESSION['idmembre']));
                    $recupidville->execute(array('idmembre' => $_SESSION['idmembre']));

                    $donnees = $recupclassville->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupidville->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }else{
                        echo $i;
                        echo "<br>";
                      }
                      
                    }
                  }
                  ?>
                </div>

                <div class="col-md-4 col-xs-4 subrank">
                  <?php 
                  if (isset($_GET['ranking']) AND $_GET['ranking']==1){
                    $recuppseudo->execute();

                    while ($donnees = $recuppseudo->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['pseudo']; 
                        echo "<br>";
                      }

                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==2){

                    if (isset($_GET['idgroup'])){
                      $recuppseudogroup->execute(array('idgroupe' => $_GET['idgroup']));

                      while ($donnees = $recuppseudogroup->fetch()){
                        if ($donnees['idmembre']==$_SESSION['idmembre']) {
                          ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                          echo "<br>";
                        }else{
                          echo $donnees['pseudo']; 
                          echo "<br>";
                        }

                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==3){
                    $recuppseudoentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recuppseudoentreprise->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['pseudo']; 
                        echo "<br>";
                      }

                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==4){
                    $recuppseudoville->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recuppseudoville->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['pseudo']; 
                        echo "<br>";
                      }

                    }
                  }
                  ?>
                </div>

                <div class="col-md-4 col-xs-4 subrank">
                  <?php
                  if (isset($_GET['ranking']) AND $_GET['ranking']==1){
                    $recupscore->execute();

                    while ($donnees = $recupscore->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['score']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['score']; 
                        echo "<br>";
                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==2){
                    if (isset($_GET['idgroup'])){
                      $recupscoregroup->execute(array('idgroupe' => $_GET['idgroup']));

                      while ($donnees = $recupscoregroup->fetch()){
                        if ($donnees['idmembre']==$_SESSION['idmembre']) {
                          ?><b><font size="4"><?php echo $donnees['score_groupe']; ?></font></b><?php  
                          echo "<br>";
                        }else{
                          echo $donnees['score_groupe']; 
                          echo "<br>";
                        }
                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==3){
                    $recupscoreentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recupscoreentreprise->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['score']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['score']; 
                        echo "<br>";
                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==4){
                    $recupscoreville->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recupscoreville->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['score']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['score']; 
                        echo "<br>";
                      }
                    }
                  }
                  ?>
                </div>

              </div>



              <!-- jQuery first, then Bootstrap JS. -->
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
            </div>
          </body>
          </html>
