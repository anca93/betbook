<!DOCTYPE html>
<html>

<?php

/*
session_start();
$pseudo = $_SESSION['pseudo'];
$idmembre = $_SESSION['idmembre'];
*/

$pseudo = "sedluap";
$idmembre = 675241;

/* Tableau  qui contiendra les erreurs de saisies */
$errors = array();

if ( isset($_POST["submit"]) ) {

    if ( strlen(   $_POST["question"]   ) <10){
        $errors["question"][] = "Non valid question (less than 10 characters)." ;
    }
    if ( substr($_POST["question"] , strlen($_POST["question"])-1 ) != '?'){
        $errors["question"][] = "Non valid question (no question mark)." ;
    }
    if ( strlen(   $_POST["answer1"]   ) <3){
        $errors["answer1"][] = "Non valid answer 1 (less than 3 characters)." ;
    }
    if ( strlen(   $_POST["answer2"]   ) <3){
        $errors["answer1"][] = "Non valid answer 2 (less than 3 characters)." ;
    }
    if ( (strlen(   $_POST["answer3"]   ) <3)&&(strlen(   $_POST["answer3"]   ) >0)){
        $errors["answer1"][] = "Non valid answer 3 (less than 3 characters)." ;
    }
    if ( (strlen(   $_POST["answer4"]   ) <3)&&(strlen(   $_POST["answer4"]   ) >0)){
        $errors["answer1"][] = "Non valid answer 4 (less than 3 characters)." ;
    }
    if ( (strlen(   $_POST["answer5"]   ) <3)&&(strlen(   $_POST["answer5"]   ) >0)){
        $errors["answer1"][] = "Non valid answer 5 (less than 3 characters)." ;
    }


    if ( (strlen(   $_POST["day"]   )!=10)||( preg_match(" #^([0-3][0-9]})(/)([0-9]{2,2})(/)([0-3]{2,2})$# ", $_POST["day"] ))){
        $errors["question"][] = "Non valid date (must be dd/mm/yyyy)." ;
    }

    
    if (count($errors) == 0) {
        // Pas d'erreurs de saisies
        include("bet_form_target.php");
        die();
    }
}


?>



<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/BETBOOK/betbook/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" media="screen" type="text/css" title="Design" href="calendar.css" />
    <link href="bet_form.css" rel="stylesheet">
    <script type="text/javascript" src="calendar.js"></script>       
    <title>BetBook : New Bet</title>
</head>
<body>




    <div class="container">
        <h2><strong> New bet </strong></h2>
        <h4> ... and new chance to master the race !</h4>
        <form method="post"  >              <!-- action="bet_form_target.php" -->


            <i>Please fill out this form to make that bet live.</i>
        </br>
    </br>




    <?php
// Si des erreurs ont été trouvées, les afficher sous forme de liste
    if (count($errors) > 0) {
        echo "<div class=\"error\">";
        echo "<ul>";
        foreach ($errors as $champEnErreur => $erreursDuChamp) {
            foreach ($erreursDuChamp as $erreur) {
                echo "<li>".$erreur."</li>";
            }
        }
        echo "</ul>";
        echo "</div>";
    }
    ?>


</br>


<div class="row">

    <div class="col-sm-12">

        Ask a question
        <input type="text" name="question" placeholder="?" class="form-control input-lg" />
    </br>

    Define the possible answers (up to 5)
    <div class="input-group">
        <span class="input-group-addon">1</span> 
        <input type="text" name="answer1" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">2</span>
        <input type="text" name="answer2" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">3</span> 
        <input type="text" name="answer3" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">4</span> 
        <input type="text" name="answer4" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">5</span> 
        <input type="text" name="answer5" class="form-control">      
    </div>

</br>



Add a few details

<textarea name="details" rows="2" class="form-control input-sm"></textarea>
</br>


<div class="row">
    <div class="col-xs-offset-1 col-xs-4">
        Locate this bet ? 
        <div class="input-group">
            <span class="input-group-addon">
                <input type="checkbox" name="city_check">
            </span>
            <input type="text" name="city" placeholder="City" class="form-control">
        </div>
        <br/>
        Tag a firm ? 
        <div class="input-group">
            <span class="input-group-addon">
                <input type="checkbox" name="firm_check">
            </span>
            <input type="text" name="firm" placeholder="Company" class="form-control">
        </div>
        <br/>

    </div>
    <div class="col-xs-offset-2 col-xs-4">


        End of bet: </br>
        <input type="text" name="day" id="day" class="calendrier" size="8" />
    </br> at </br>
    <select name="hour">
        <?php

        $end = new Datetime('22:00');
        for ($t =  new Datetime('08:00'); $t <= $end; $t->modify('+30 minutes')) {
           $value = $t->format('H:i');
           echo '<option value="' . $value . '">' . $value . '</option>';
       }?>
   </select>

</br></br>

</div>
</div> 


</br>
</div>


<strong></br><h4><center>
    Ready to rule the world ?
</br>
<button type="submit" name="submit" class="btn btn-primary btn-lg"> Submit </button>
</strong></h4></center>
</br>               
</body>
</html>