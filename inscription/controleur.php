
<?php

	
// Afficher les erreurs à l'écran
ini_set('display_errors', 1);
// Afficher les erreurs et les avertissements


	include_once('membre.class.php');

	if(isset($_POST["pseudo"]) AND isset($_POST['mdp']) AND isset($_POST['email'])){
		$invalide=false;

		//validité de l'adresse mail:
		if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {	

				$invalide=true;
				header('Location: http://localhost/projet_web/inscription/inscription.php?mail_invalide=0');
		}

		//validité du mot de passe
		if (preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['mdp'])===0){

			//echo 'mot de passe correct';
			$invalide=true;

			header('Location: http://localhost/projet_web/inscription/inscription.php?mdp_invalide=0');

		}

		//validite du pseudo

		if (preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['pseudo'])===0){
			$invalide=true;
			
			header('Location: http://localhost/projet_web/inscription/inscription.php?pseudo_invalide=0');
			
		}

			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['birth'])===0)
	    {
	    	$invalide=true;
	     
	        header('Location: http://localhost/projet_web/inscription/inscription.php?date_invalide=0');

	    }

	    $var1=preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['pseudo']);
	    $var2=preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['mdp']);
	    $var3=filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);

	    echo 'preg_match pseudo: '.$var1;
	    echo 'preg_match mdp: '.$var2;
	    echo 'filter_var(email): '.$var3;



		// Hachage du mot de passe
		$pass_hache = sha1($_POST['mdp']);
		//ouvre la base de données

		try{

		$bdd= new PDO('mysql:host=localhost;dbname=projet','root','busi',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
		catch(Exception $e){
			 die('Erreur : '.$e->getMessage());
		}

		//on regarde tout les autres pseudo et email
        $reponse = $bdd->query('SELECT pseudo AS pseudos,mail AS email  FROM Membre');

        //on compare avec les autres pseudoset email.

        $retour=false;

		while ($donnees = $reponse->fetch()){

			if($donnees['pseudos']==$_POST["pseudo"]) {
			    	header('Location: http://localhost/projet_web/inscription/inscription.php?pseudo_utilise=0');
			    	$retour=true;
			    	break;
			}
			if($donnees['email']==$_POST["email"]) {
			    	header('Location: http://localhost/projet_web/inscription/inscription.php?email_utilise=0');
			    	$retour=true;
			    	break;
			}


		}



		if(($retour==false) AND ($invalide==false)){
			
			$membre=new Membre();
			$membre->setPseudo($_POST['pseudo']);
			$membre->setMdp($pass_hache);
			$membre->setEmail($_POST['email']);
			$membre->setAnniversaire($_POST['birth']);
			$membre->setVille($_POST['ville']);
			$membre->setEntreprise($_POST['entreprise']);
			$pseudo=$membre->getPseudo();
			$mdp=$membre->getMdp();
			$email=$membre->getEmail();
			$ville=$membre->getVille();
			$anniversaire=$membre->getAnniversaire();
			$entreprise=$membre->getEntreprise();
			

			$req = $bdd->prepare('INSERT INTO Membre(pseudo, password, mail,ville, birthday,entreprise,picture,score) VALUES(:pseudo, :password, :mail, :ville, :birthday, :entreprise, :picture, :score)');
	        $req->execute(array(
	        'pseudo' => $pseudo,
	        'password' => $mdp,
	        'ville' =>$ville,
	        'birthday'=>$anniversaire,
	        'entreprise'=>$entreprise,
	        'mail' => $email,
	        'picture'=>'profil_vide.jpg',
	        'score'=>0));
	        echo 'fin';	

	       

        	$connect = $bdd->prepare('SELECT idmembre,picture,birthday, ville, mail, entreprise, score FROM Membre WHERE pseudo = :pseudo AND password = :password');
        	$connect->execute(array(
        	'pseudo' => $pseudo,
        	'password' => $pass_hache));

        	

        	$resultat = $connect->fetch();

			session_start();
            $_SESSION['idmembre'] = $resultat['idmembre'];
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['image'] =$resultat['picture'];
            $_SESSION['birthday'] =$resultat['birthday'];
            $_SESSION['ville'] =$resultat['ville'];
            $_SESSION['mail'] =$resultat['mail'];
            $_SESSION['entreprise'] =$resultat['entreprise'];
            $_SESSION['score'] =$resultat['score'];
            

            

			header('Location: http://localhost/projet_web/default_page/default_page.php');

		}

	}
?>