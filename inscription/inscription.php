<?php

session_destroy();

?>

<!DOCTYPE html>

<html>

    <head>

    <meta charset="UTF-8">
    <title>BetBook-The social betting</title>   
    <link rel="stylesheet" href="style.css" /> 
    <link rel="icon" href="logo2.ico" />

    </head>

    <body>
        <!-- Bandeau--> 
        <header>

            <img src="logo2.jpg" width="130" height="80" alt="" title="" style="float:left;"></img>

            <div class="texte_info" ><p> JOUER COMPORTE DES RISQUES:ENDETTEMENT,ISOLEMENT,DEPENDANCE </br>
             POUR ÊTRE AIDE, APPELEZ LE 0800 801 381 (SOS JEU)</p>
            </div>

            <div class="headerLoginBox" style="display:block;">

            <!--*************************Connexion********************************-->

                <form action="controleur_login.php" method="post">

                <p>     

                <label for="pseudo_login">Pseudo:</label><input type="text" name="pseudo_login" />
                <label for="mdp_login">Mot de Passe:</label><input type="password" name="mdp_login" />
                <button type="submit">LOGIN</button>    
            
                </p>
                </form>
                <?php
                if (isset($_GET['connexion']) AND ($_GET['connexion']==0 )){
                    
                   echo '
                        <p><span class=password_login>Mot de passe ou login incorrect<span></p>
                     ';   
                }
                ?>
                

            </div> 
        </header>


        <!-- Corps--> 
        <div id="corps">

            <!-- Gauche--> 

            <div id="gauche"> 

                <h1> Un site de pari entre collègues, amis, étudiants...</br>
            
                </h1>
            </div>

            <!-- Droite--> 

            <div id="droite">
            
                <strong><p><span class="titre">Veuillez remplir les champs suivants pour vous inscrire</p></strong>

                <form action="controleur.php" method="post">


                    <!--Alignement des boutons-->

                    <!--******************Inscription**********************************-->
                 
                    <div class="aligne" >   
                        <p> 
                        <label for="pseudo">Pseudo*:</label><input type="text" name="pseudo" placeholder="Login" />
                        </br>
                        </br>
                        <label for="mdp">Mot de Passe*:</label><input type="password" name="mdp" placeholder="Mot de Passe"/>
                        </br>
                        </br>
                        <label for="email">email*:</label><input type="text" name="email" placeholder="@mail" />
                        </br>
                        <label for="birth">Anniversaire*</label><input type="text" name="birth"  placeholder="aaaa-mm-jj" />
                        </br>
                        </br>
                        <label for="ville">Ville:</label><input type="text" name="ville" placeholder="Ville" />
                        </br>
                        </br>
                        <label for="entreprise">Entreprise/Ecole</label><input type="text" name="entreprise" placeholder="Entreprise/Ecole" />
                        </br>
                        </br>
                        <button type="submit">ENVOYER</button>
                        </p>

                    </div>

                </form>

                <div class="obligatoire">
                    <p>Les champs marqué par * sont obligatoires</p>
                </div>


                <!--*************champs incorrects************-->

                <?php
                if (isset($_GET['mail_invalide']) AND ($_GET['mail_invalide']==0 )){
                    
                   echo '
                        <p><span class=email_utilise>Adresse Email incorrect<span></p>
                     ';   
                }
                if (isset($_GET['mdp_invalide']) AND ($_GET['mdp_invalide']==0 )){
                    
                   echo '
                        <p><span class=wrong_password>entre 4 et 20 caractères, sans caractères spéciaux<span></p>
                     ';   
                }

                if (isset($_GET['pseudo_invalide']) AND ($_GET['pseudo_invalide']==0 )){
                    
                   echo '
                        <p><span class=pseudo_invalide>entre 4 et 15 caractères, sans caractères spéciaux<span></p>
                     ';   
                }
                if (isset($_GET['email_utilise']) AND ($_GET['email_utilise']==0 )){
                    
                   echo '
                        <p><span class=email_utilise> Cet email est déjà utilisé <span></p>
                     ';   
                }
                if (isset($_GET['pseudo_utilise']) AND ($_GET['pseudo_utilise']==0 )){
                    
                   echo '
                        <p><span class=pseudo_invalide> Le pseudo est déjà utilisé <span></p>
                     ';   
                }

                if (isset($_GET['date_invalide']) AND ($_GET['date_invalide']==0 )){
                    
                   echo '
                        <p><span class=date_invalide>doit être au format yyyy-mm-dd <span></p>
                     ';   
                }

                ?>


            </div>
            </div>
        </div>
             
    </body>

</html>