-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 21 Mai 2016 à 18:09
-- Version du serveur :  10.1.10-MariaDB
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `BetBook`
--

-- --------------------------------------------------------

--
-- Structure de la table `Groupe`
--

CREATE TABLE `Groupe` (
  `idgroupe` int(11) NOT NULL,
  `nom_groupe` varchar(255) NOT NULL,
  `creation_date_groupe` date NOT NULL,
  `idadmin_groupe` int(11) NOT NULL,
  `description_groupe` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Groupe`
--

INSERT INTO `Groupe` (`idgroupe`, `nom_groupe`, `creation_date_groupe`, `idadmin_groupe`, `description_groupe`) VALUES
(1, 'Theo''s group', '2016-05-09', 1, 'This is Theo''s group'),
(2, 'Guillaume''s group', '2016-05-21', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `Invitation`
--

CREATE TABLE `Invitation` (
  `idinvitation` int(11) NOT NULL,
  `idmembre_invitation` int(11) NOT NULL,
  `idgroupe_invitation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Invitation`
--

INSERT INTO `Invitation` (`idinvitation`, `idmembre_invitation`, `idgroupe_invitation`) VALUES
(1, 1, 2),
(2, 1, 1),
(3, 2, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Jonction`
--

CREATE TABLE `Jonction` (
  `idjonction` int(11) NOT NULL,
  `idmembre` int(11) NOT NULL,
  `idgroupe` int(11) NOT NULL,
  `score_groupe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Jonction`
--

INSERT INTO `Jonction` (`idjonction`, `idmembre`, `idgroupe`, `score_groupe`) VALUES
(1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Membre`
--

CREATE TABLE `Membre` (
  `idmembre` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `ville` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `entreprise` varchar(255) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Membre`
--

INSERT INTO `Membre` (`idmembre`, `pseudo`, `password`, `picture`, `birthday`, `ville`, `mail`, `entreprise`, `score`) VALUES
(1, 'Theo', 'test', '‰PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0T\0\0\06\0\0\0YW¾\0\06iCCPICC Profile\0\0X…•y	8Uß×ÿ>÷ÜÁå^ó<Ïó<™ç)ó¬t]Ó5g&’!…Š&"S’)\ZHB)‘!DJ¥T*J™Þƒêû{¿ïÿù¿Ï»Ÿçœó±öÚkö^kË€‹Œ¢ $4*ÂÞÔ€ßÕÍ7 @\0Ì€IäÈp}[[+€”?ßÿ^¾"ÚH–Ù´õ?ëÿ¿…ÁÇ7’\0d‹`oŸHr‚¯€æ$‡GD€yŒÈ…b£Â7ñ"‚™#‚\0`©6±ÿ6æÞÄÞÛX~KÇ', '2016-05-10', 'Limoges', 'theo.deprelle@gmail.com', 'Une ferme', 400),
(2, 'fefencore', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'profil_vide.jpg', '1995-08-18', '', 'guillaumef.22@live.fr', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Paris`
--

CREATE TABLE `Paris` (
  `idparis` int(11) NOT NULL,
  `nom_paris` varchar(255) NOT NULL,
  `idadmin_paris` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `ville` varchar(255) NOT NULL,
  `entreprise` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `answer1` varchar(255) NOT NULL,
  `answer2` varchar(255) NOT NULL,
  `answer3` varchar(255) NOT NULL,
  `answer4` varchar(255) NOT NULL,
  `answer5` varchar(255) NOT NULL,
  `idgroupe` int(11) NOT NULL,
  `good_answer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Paris`
--

INSERT INTO `Paris` (`idparis`, `nom_paris`, `idadmin_paris`, `creation_date`, `end_date`, `ville`, `entreprise`, `description`, `answer1`, `answer2`, `answer3`, `answer4`, `answer5`, `idgroupe`, `good_answer`) VALUES
(1, 'Theo''s bet', '1', '2016-05-18 00:00:00', '2016-05-26 00:00:00', 'Lyon', 'BetBook', 'Who''s the best developper ? ', 'Theo', 'Anca', 'PD', 'Paul', 'Ferron', 0, 0),
(2, 'What is my name?', '675241', '2016-05-11 11:56:56', '2016-05-20 17:30:00', 'Lyon', 'NONE', 'hard question', 'Paul', 'Dès', 'Daisy', 'NONE', 'NONE', 0, 0),
(3, 'What the fuck ?', '675241', '2016-05-11 22:39:29', '2016-05-08 08:00:00', 'NONE', 'NONE', '', 'answer 1', 'good morning', 'no doubts', 'NONE', 'NONE', 0, 0),
(4, 'Why do i live?', '675241', '2016-05-11 22:56:36', '2016-05-14 08:00:00', 'NONE', 'NONE', '', 'y', 'good morning', 'good evening', 'NONE', 'NONE', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Vote`
--

CREATE TABLE `Vote` (
  `idvote` int(11) NOT NULL,
  `idparis` int(11) NOT NULL,
  `idmembre` int(11) NOT NULL,
  `idreponse_vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Vote`
--

INSERT INTO `Vote` (`idvote`, `idparis`, `idmembre`, `idreponse_vote`) VALUES
(1, 1, 1, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Groupe`
--
ALTER TABLE `Groupe`
  ADD PRIMARY KEY (`idgroupe`);

--
-- Index pour la table `Invitation`
--
ALTER TABLE `Invitation`
  ADD PRIMARY KEY (`idinvitation`);

--
-- Index pour la table `Jonction`
--
ALTER TABLE `Jonction`
  ADD PRIMARY KEY (`idjonction`);

--
-- Index pour la table `Membre`
--
ALTER TABLE `Membre`
  ADD PRIMARY KEY (`idmembre`);

--
-- Index pour la table `Paris`
--
ALTER TABLE `Paris`
  ADD PRIMARY KEY (`idparis`);

--
-- Index pour la table `Vote`
--
ALTER TABLE `Vote`
  ADD PRIMARY KEY (`idvote`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Groupe`
--
ALTER TABLE `Groupe`
  MODIFY `idgroupe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Invitation`
--
ALTER TABLE `Invitation`
  MODIFY `idinvitation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `Jonction`
--
ALTER TABLE `Jonction`
  MODIFY `idjonction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `Membre`
--
ALTER TABLE `Membre`
  MODIFY `idmembre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `Paris`
--
ALTER TABLE `Paris`
  MODIFY `idparis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `Vote`
--
ALTER TABLE `Vote`
  MODIFY `idvote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
