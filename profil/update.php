<?php

	session_start();	
	// Afficher les erreurs à l'écran
	ini_set('display_errors', 1);
	$idmembre=$_SESSION['idmembre'];
	$use=1;
	$same=0;
	// Afficher les erreurs et les avertissements

	//ouvre la base de données


		try{

		$bdd= new PDO('mysql:host=localhost;dbname=projet','root','busi',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
		catch(Exception $e){
			 die('Erreur : '.$e->getMessage());
		}


		

		//validité de l'adresse mail:
/*--------------------------------------------------------------------------------------------------------------------------*/

		if (filter_var($_POST['new_mail'],FILTER_VALIDATE_EMAIL) AND isset($_POST['new_mail'])) {	
			$reponse = $bdd->query('SELECT mail AS email  FROM Membre');

        //on compare avec les autres email.

		while ($donnees = $reponse->fetch()){

			if($donnees['email']==$_POST["new_mail"]) {
					$use=0;
			    	header('Location: http://localhost/projet_web/profil/profil_setting.php?email_utilise=0');
			    	break;
			}
		}
		if($use==1){

		$bdd->exec('UPDATE Membre SET mail="'.$_POST['new_mail'].'" WHERE idmembre ="'.$idmembre.'"'); 
		$_SESSION['mail'] =$_POST['new_mail'];
		header('Location: http://localhost/projet_web/profil/profil_setting.php');
		}


		}else if(isset($_POST['new_mail'])){

				header('Location: http://localhost/projet_web/profil/profil_setting.php?new_mail=0');

		}
		
/*-------------------------------------------------------------------------------------------------------------------------------------*/
		//validité du mot de passe
		if ((preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['old_password'])) AND isset($_POST['old_password']) AND (preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['new_password'])) AND isset($_POST['new_password'])){

			$req1 = $bdd->prepare('SELECT password FROM Membre WHERE idmembre = :idmembre');
	        $req1->execute(array(
	        'idmembre' => $idmembre));
	        $resultat1 = $req1->fetch();
	        $pass_check=sha1($_POST['old_password']);

	        if ($resultat1['password']==$pass_check) {
	        	$same=1;
	        	$pass_hache = sha1($_POST['new_password']);
	        }

	        if($same==1){
			$bdd->exec('UPDATE Membre SET password="'.$pass_hache.'" WHERE idmembre ="'.$idmembre.'"'); 
			header('Location: http://localhost/projet_web/profil/profil_setting.php?password_valid=0');
	        	
	        }else{

	        	header('Location: http://localhost/projet_web/profil/profil_setting.php?same_password=0');

	        }
			
			
		}else if(isset($_POST['new_password']) OR isset($_POST['old_password'])){

			header('Location: http://localhost/projet_web/profil/profil_setting.php?new_password=0');

		}
			
/*--------------------------------------------------------------------------------------------------------------------------------------*/			
		

		//validite du pseudo

		if ((preg_match("/^([a-zA-Z0-9]{4,15})$/",$_POST['new_pseudo'])) AND isset($_POST['new_pseudo'])){

			$answer = $bdd->query('SELECT pseudo AS pseudos  FROM Membre');

			while ($resultat = $answer->fetch()){

				if($resultat['pseudos']==$_POST["new_pseudo"]) {
						$use=0;
				    	header('Location: http://localhost/projet_web/profil/profil_setting.php?pseudo_utilise=0');
				    	
				    	break;
				}
			}

			if($use==1){

			$bdd->exec('UPDATE Membre SET pseudo="'.$_POST['new_pseudo'].'" WHERE idmembre ="'.$idmembre.'"'); 
			$_SESSION['pseudo'] =$_POST['new_pseudo'];
			header('Location: http://localhost/projet_web/profil/profil_setting.php');
			}
				
		}else if(isset($_POST['new_pseudo'])){

			header('Location: http://localhost/projet_web/profil/profil_setting.php?new_pseudo=0');

		}

/*--------------------------------------------------------------------------------------------------------------------------------------*/

		//validite de la date d'anniversaire

			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['new_birthday']) AND isset($_POST['new_birthday'])){


			$bdd->exec('UPDATE Membre SET birthday="'.$_POST['new_birthday'].'" WHERE idmembre ="'.$idmembre.'"'); 
			$_SESSION['birthday'] =$_POST['new_birthday'];
			header('Location: http://localhost/projet_web/profil/profil_setting.php');





	    	}else if(isset($_POST['new_birthday'])){
	    		header('Location: http://localhost/projet_web/profil/profil_setting.php?new_birthday=0');


	    	}
/*--------------------------------------------------------------------------------------------------------------------------------------*/

	    // Ville

	    	if (preg_match("/^([a-zA-Z0-9]{2,30})$/",$_POST['new_city'])AND isset($_POST['new_city'])){


			$bdd->exec('UPDATE Membre SET ville="'.$_POST['new_city'].'" WHERE idmembre ="'.$idmembre.'"');
			$_SESSION['ville'] =$_POST['new_city'];
			header('Location: http://localhost/projet_web/profil/profil_setting.php');




	    	}else if(isset($_POST['new_city'])){
	    		header('Location: http://localhost/projet_web/profil/profil_setting.php?new_city=0');


	    	}
/*-----------------------------------------------------------------------------------------------------------------------------------------*/
	    //Company


	    	if (preg_match("/^([a-zA-Z0-9]{2,30})$/",$_POST['new_university']) AND isset($_POST['new_university'])){


			$bdd->exec('UPDATE Membre SET entreprise="'.$_POST['new_university'].'" WHERE idmembre ="'.$idmembre.'"'); 
			$_SESSION['entreprise'] =$_POST['new_university'];
			header('Location: http://localhost/projet_web/profil/profil_setting.php');


	    	}else if(isset($_POST['new_university'])){
	    		header('Location: http://localhost/projet_web/profil/profil_setting.php?new_university=0');


	    	}
?>