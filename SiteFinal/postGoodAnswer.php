<?php

	/*** Doit rentrer la bonne réponse dans le champ good_answer de la table Paris ***/


	session_start();

	//variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    
    //verifie l'acces au pari comme admin
   	//**********************************************************************************//   
   	
   	$getParis->execute(array($_POST['idParis']));
    $donneesParis = $getParis->fetch();

    if($idMembre != $donneesParis['idadmin_paris'])
   	{
   		header('Location: main.php'); 
   	}
  	//**********************************************************************************//


    //analyse de answer
	if(isset($_POST['answer']) AND isset($_POST['idParis']))
	{
		
		$_POST['answer'] = (int)htmlspecialchars($_POST['answer']);
		$_POST['idParis'] = (int)htmlspecialchars($_POST['idParis']);

		if($_POST['answer'] > 5 or $_POST['answer'] < 0)
		{
			header('Location: main.php'); 
		}


		$getParis->execute(array($_POST['idParis']));
		$donneesParis = $getParis->fetch();
		
		if ($donneesParis['good_answer'] == 0)
		{
			$addGoodAnswer->execute(array($_POST['answer'], $_POST['idParis']));


			$getUsers->execute(array($_POST['idParis']));

			while( $users = $getUsers->fetch())
			{
				$getAnswer->execute(array($users['idmembre'], $_POST['idParis']));
				$idAnswer = $getAnswer->fetch();
				if( $idAnswer['idreponse_vote'] == $_POST['answer'])
				{
					
    				$getScoreUser->execute(array($users['idmembre']));
    				$score = $getScoreUser ->fetch();
					$newscore = $score['score'] + 1;
					$updateScore->execute(array($newscore,$users['idmembre']));

					$getScoreGroupe->execute(array($users['idmembre'], $donneesParis['idgroupe']));
					$scoreGroupe = $getScoreGroupe ->fetch();
					$newscoreGroupe = $scoreGroupe['score_groupe'] + 1;
					$updateScoreGroupe->execute(array($newscoreGroupe,$users['idmembre'],$donneesParis['idgroupe']));
				}
		
			}	
			$getUsers->closeCursor();
		}

		header('Location: bet.php?id='.$_POST['idParis']); 
	}
	else
	{
		header('Location: main.php'); 
	} 
?>