<?php
    
    
    if(session_start()==0 || $_SESSION['idmembre'] ==0)
    {
        header('Location: inscription.php'); 
        exit();
    }
    //variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //**********************************************************************************//

    //Definition et calcul des erreurs dans le formulaire
    $errors = array();

    if ( isset($_POST["submit"]) ) {

        if ( strlen(   $_POST["name"]   ) < 5 || strlen(   $_POST["name"]   ) > 25){
            $errors["name"][] = "Non valid name (not between 5 and 25 characters)." ;
        }

        if (count($errors) == 0) {
            // Pas d'erreurs de saisies
            include("groupe_created.php");
            die();
        }
    }
    
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>BetBook|The social betting</title> 
      <link rel="icon" href="utils/image/logo.ico" />

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">
    <!-- Autocomplete field -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <!-- Error -->
    <link rel="stylesheet" href="bootstrap/css/error.css">
        
    <title>BetBook : New Group</title>

</head>
<body>
<div class="corps">


 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">   

            <div class="col-md-12 col-xs-12 bg">

                <div class="container">

                    <div class="row">


                        <h1 class="titre"> New Group </h1>

                    </div>

                <form method="post">

                <i>Please fill out this form to create the group.</i>

                <?php
                // Si des erreurs ont été trouvées, les afficher sous forme de liste
                    if (count($errors) > 0) {
                        echo "<div class=\"error\">";
                        echo "<ul>";
                        foreach ($errors as $champEnErreur => $erreursDuChamp) {
                            foreach ($erreursDuChamp as $erreur) {
                                echo "<li>".$erreur."</li>";
                            }
                        }
                        echo "</ul>";
                        echo "</div>";
                    }
                ?>



                <div class="row">

                    <div class="col-sm-12">

                        Name
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" />
                        </div>

                        Description
                        <div class="form-group">
                            <textarea name="details" rows="2" class="form-control"></textarea>
                        </div>

                        Add friends

                        <div class="ui-widget">
                            <input name="friend" id="keyword" placeholder="Search friends" type="text" class="form-control"/>
                        </div> 

                    </br>

                </div>


        <center>
            
        <button type="submit" name="submit" class="btn btn-primary btn-lg"> Submit </button></center>
</br>       
           
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<!-- script "Autocomplete Multiple Values" pour la recherche des amis -->
<script>
    $(function() {
        function split( val ) {
            return val.split( /;\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( "#keyword" ).bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 1,
            source: function( request, response ) {
            //extraction des valeurs envoyes par la bdd
            $.getJSON("utils/autocomplete_pseudo_return.php", { term : extractLast( request.term )},response);
        },
        focus: function() {
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            // enlevement input actuel
            terms.pop();
            // rajout du element selectionne
            terms.push( ui.item.value );
            // rajout du "; " apres chaque nom
            terms.push( "" );
            this.value = terms.join( "; " );
            return false;
        }
    });
    });
</script>
</div>
</body>
</html>
