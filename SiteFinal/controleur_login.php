<?php
    ini_set('display_errors', 1);

    //include des request sql et la bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    // on teste si nos variables sont définies
    if (isset($_POST['pseudo_login']) && isset($_POST['mdp_login'])) {

        $pseudo=$_POST['pseudo_login'];
        $pass_hache = sha1($_POST['mdp_login']);

        // Vérification des identifiants
        $req = $bdd->prepare('SELECT idmembre, picture, birthday, ville, mail, entreprise, score FROM Membre WHERE pseudo = :pseudo AND password = :password');
        $req->execute(array(
        'pseudo' => $pseudo,
        'password' => $pass_hache));


        $resultat = $req->fetch();

        if (!$resultat){


            header('Location: inscription.php?connexion=0');

         
        }

        else{
            session_start();
            $_SESSION['idmembre'] = $resultat['idmembre'];
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['image'] =$resultat['picture'];
            $_SESSION['birthday'] =$resultat['birthday'];
            $_SESSION['ville'] =$resultat['ville'];
            $_SESSION['mail'] =$resultat['mail'];
            $_SESSION['entreprise'] =$resultat['entreprise'];
            $_SESSION['score'] =$resultat['score'];
            header('Location: main.php');

        }
    }
    ?>
