<?php

	if(session_start()==0 || $_SESSION['idmembre'] ==0)
	{
		header('Location: inscription.php'); 
		exit();
	}

    //variable à initialiser avec les variable de scession
    //**********************************************************************************//    
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//
    

    //includes des requetes php et connection bdd
    //**********************************************************************************//
    include("utils/sqlrequest.php");
    //**********************************************************************************//

    //analyse de la la variable $_GET['id'] du groupe
    //**********************************************************************************//
    if(isset($_GET['id']))
    {
    	$_GET['id'] = (int)htmlspecialchars($_GET['id']);

    	if($_GET['id'] <= 0)
    	{
    		header('Location: main.php'); 
    	}
    }
    
    //**********************************************************************************//

    //analyse de la la variable $_GET['page'] pour la pagination des paris
  	//**********************************************************************************//
	$nbrParis = 0;
	$randomBetsUnlimited->execute();
    while($donnees = $randomBetsUnlimited->fetch())
    {
    	$nbrParis += 1;
    }
 	$randomBetsUnlimited->closeCursor();

	if(isset($_GET['page']))
	{
		$_GET['page'] == (int)htmlspecialchars($_GET['page']);
		
		if($_GET['page'] > 0)
		{
			if($_GET['page']*4 <= $nbrParis)
			{
				$numPage = $_GET['page'];
			}
			else
			{
				$numPage = 0;
			}
		}
		else
		{
			$numPage = 0;
		}
	}
	else
	{
		$numPage = 0;
	}
    $num = 1+4*$numPage;
    $betInit = $numPage*4;

     //analyse de la la variable $_GET['page'] pour la pagination des paris
  	//**********************************************************************************//
	$nbrMembre = 0;
	$getIdGroupe->execute(array($_GET['id']));
    while($donnees = $getIdGroupe->fetch())
    {
    	$nbrMembre += 1;
    }

 	$getIdGroupe->closeCursor();

	if(isset($_GET['profil']))
	{
		$_GET['profil'] == (int)htmlspecialchars($_GET['page']);
		
		if($_GET['profil'] > 0)
		{
			if($_GET['profil'] <= $nbrMembre-4)
			{
				$profilInit = (int)$_GET['profil'];
			}
			else
			{
				$profilInit = 0;
			}
		}
		else
		{
			$profilInit = 0;
		}
	}
	else
	{
		$profilInit = 0;
	}	
  	//**********************************************************************************//


  	//verifie l'acces au groupe
   	//**********************************************************************************//   
   	$present = 0;
   	
   	$header->execute();
    while($donneesGroupe = $header->fetch())
    {
   		if($_GET['id'] == $donneesGroupe['idgroupe'])
    	{
    		$present = 1;
	    }
    }
   	$header->closeCursor();   	
   	if($present != 1)
   	{
   		header('Location: access_groupe.php?id='.$_GET['id']); 
   	}
  	//**********************************************************************************//

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    
    <title>BetBook|The social betting</title>  
      <link rel="icon" href="utils/image/logo.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>
<div class="corps">

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">
	       		<div class="col-md-12 col-xs-12 bg">
	       			
	       			<div class="container">

		       			<div class="row">
		       				<?php
		       				$getGroup->execute(array($_GET['id']));
		       				$group = $getGroup->fetch();
		       				?>
		        			<h1 class="titre"><?php echo "#"; echo  $group['nom_groupe']; ?></h1>
		        		
		        		<div class="row">
		        		
			        		<div class="col-md-12 col-xs-12">

			        			<i> <?php echo  $group['description_groupe'] ?></i>

			        			<?php
			        				//verifie qu'il n'y a pas plus de 5 créations dans la journée
								    //********************************************************************************//           
								    $creationmax->execute(array('idmembre'=>$_SESSION['idmembre']));
								    $donneesmax=$creationmax->fetch();
								    

								    if($donneesmax['max']>=5){

								    ?>

								    <button type="button" name='idgroup' value= '<?php echo $_GET['id']?>' class="btn btn-primary btn-lg btn-block shadow" data-toggle="modal" data-target="#myModal">
											Post a bet
										</button>

											<!-- Modal -->
											<div id="myModal" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Limit reached</h4>
											      </div>
											      <div class="modal-body" style="text-align:center;">
											        <p>You have already posted 5 bets today. You can post new bets tomorrow.</p>
											      </div>
											      
											    </div>

											  </div>
											</div>
								     
								    <?php
								    } else {
								    ?>
								    	<form action = "new_bet_form.php" method="get" >
											<button type="submit" name='idgroup' value= '<?php echo $_GET['id']?>' class="btn btn-primary btn-lg btn-block shadow">
												Post a Bet
											</button>
										</form>
									<?php
									}
									?>

								
							</div>

<!--**************************************************************** Bets **********************************************************************-->
					
						</div> 
						</br>
						<h4> Member: </h4>
						<div class="row">
							<div class="col-xs-1">
								<?php
									if($profilInit != 0)
									{
								?>
								<a href="groupe.php?profil=<?php echo $profilInit-1.?>&id=<?php echo$_GET['id']?>">
									</br>
									<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>
								</a>
								<?php
									}
								?>
							</div>
							<div class="col-xs-10">
							<?php
								$getIdGroupe4->execute();
								while($idMembre = $getIdGroupe4->fetch())
								{
									$getUsersData->execute(array($idMembre['idmembre']));
									$membre = $getUsersData->fetch();
									?>
										<div class="col-xs-3" style="text-align: center; margin-top: 10px; ">
											<a href="profil_public.php?pseudo=<?php echo $membre['pseudo']?>" >
											<img src="utils/image/<?php echo $membre['picture'] ?>" class = "img-circle profile" </img>
											</br>
											</a>
										<?php echo $membre['pseudo']?>
										</div>
									<?php
								}
							?>
							</div>
							<div class="col-xs-1">
							<?php
								if($profilInit+4 < $nbrMembre)
								{
							?>
								<a href="groupe.php?profil=<?php echo $profilInit+1?>&id=<?php echo$_GET['id']?>">
									</br>
									<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>
								</a>
							<?php
								}
							?>
							</div>
						</div>
						</br><h4> Bets: </h4>

						<div class="row">

		        		<?php
		        				$groupeBetsLimit->execute();

							    while($donneesBets = $groupeBetsLimit->fetch())
							    {
							    	$end_date = mktime( $donneesBets['end_date'][11].$donneesBets['end_date'][12], 
														$donneesBets['end_date'][14].$donneesBets['end_date'][15],
														$donneesBets['end_date'][17].$donneesBets['end_date'][18],
														$donneesBets['end_date'][5].$donneesBets['end_date'][6],
														$donneesBets['end_date'][8].$donneesBets['end_date'][9],
														$donneesBets['end_date'][0].$donneesBets['end_date'][1].$donneesBets['end_date'][2].$donneesBets['end_date'][3]);

							    	// si end_date du paris est plus vieux d'une semaine, on l'efface de la bdd et on l'affiche pas

							    	if(  $end_date + 604800 < time()   )	
							    	{
							    		$deleteBet -> execute(array($donneesBets['idparis']));
    									$deleteVote -> execute(array($donneesBets['idparis']));
    									$deleteVote -> closeCursor();
    									$deleteBet -> closeCursor();
							    	}
							    	else{
							    	?>
								    	<div class="col-md-6  col-xs-12">
								    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
									    		<div class="row">
									    			<div class="col-md-12 col-xs-12 bg-primary center_text">
									    				<div class="row">
											    			<div class="col-md-4 col-xs-3 num textBet">
									    						<i class="fa fa-tag" aria-hidden="true"></i>
																n°<?php 
									    							echo $num;
									    							$num += 1;
									    						?>								    					
									    					</div>
									    				</div>
											    			
											    	</div>

									    			<div class="col-md-12 col-xs-12 bg-primary">
											    		<h5>
											    	<?php
											    	 	echo $donneesBets['nom_paris'];
											    	?>
											    		</h5>
											    		
											    	</div>
											    	<div class="col-md-12 col-xs-12 bg-primary">

											    		<?php
											    		$donneesParis = $getParis->fetch();
										    			$currentTime = time();
												 		$endTime = mktime( $donneesBets['end_date'][11].$donneesBets['end_date'][12], 
																									$donneesBets['end_date'][14].$donneesBets['end_date'][15],
																									$donneesBets['end_date'][17].$donneesBets['end_date'][18],
																									$donneesBets['end_date'][5].$donneesBets['end_date'][6],
																									$donneesBets['end_date'][8].$donneesBets['end_date'][9],
																									$donneesBets['end_date'][0].$donneesBets['end_date'][1].$donneesBets['end_date'][2].$donneesBets['end_date'][3]);

										    			$remainingTime = $endTime - $currentTime;

										    			?>

											    		<form method="post" action="bet.php?id=<?php echo $donneesBets['idparis']?>">
											    				
											    					<div class="row">

											    				<?php 
											    				if($remainingTime>0){	
											    				?>	
											    				<button type"submit" class="btn btn-info btn-lg no_radius btn-block" >	  
													    		Bet now!
													    		<?php
													    		}else{
													    		?>
													    		<button type"submit" class="btn btn-warning btn-lg no_radius btn-block" >	  
													    		See results
													    		<?php
													    		}
													    		?>


													    </button>
													    </div>



											    		</form>


											    	</div>

<!--**************************************************************** info bet **********************************************************************-->

											    	<div class="col-md-12 col-xs-12">
														<div class="row">
															<div class="col-md-6 col-xs-6">
																<?php
																 $pseudo;
																 $getNameUser->execute(array($donneesBets['idadmin_paris']));
											    				 while($name = $getNameUser->fetch())
								    							 {
								    							 		$pseudo = $name['pseudo'];
								    							 }
								    							 $getNameUser->closeCursor();
																?>
																<a href="profil_public.php?pseudo=<?php echo $pseudo?>">
													    			<i class="fa fa-user fa-fw center_text textBet"></i>
													    			<?php
													    			
										    							 		echo $pseudo;
													    			?>
													    		</a>
													    	</div>
													    	<div class="col-md-6 col-xs-6 text-primary center_text textBet">
													    		<i class="fa fa-clock-o fa-fw"></i>

													    			<?php
										    							 		
										    							 		$donneesParis = $getParis->fetch();
										    							 		$currentTime = time();
										    							 		$endTime = mktime( $donneesBets['end_date'][11].$donneesBets['end_date'][12], 
																									$donneesBets['end_date'][14].$donneesBets['end_date'][15],
																									$donneesBets['end_date'][17].$donneesBets['end_date'][18],
																									$donneesBets['end_date'][5].$donneesBets['end_date'][6],
																									$donneesBets['end_date'][8].$donneesBets['end_date'][9],
																									$donneesBets['end_date'][0].$donneesBets['end_date'][1].$donneesBets['end_date'][2].$donneesBets['end_date'][3]);

										    							 		$remainingTime = $endTime - $currentTime;
										    							 		if($remainingTime>0){
										    							 			$rd = (int)($remainingTime / 86400); //jours
										    							 			$rh = (int)(($remainingTime - ($rd*86400))/3600); //heures
										    							 			if($rd>0){
										    							 				echo $rd.' days | '.$rh.' hours';
										    							 			}else{
										    							 				echo $rh.' hours';
										    							 			}
										    							 		}else{
										    							 			echo '<font color="orange">End date reached !</font>';
										    							 		}


													    			?>
													    	</div>
													    </div>
											    	</div>
											</div>
										    	<div class="col-md-12 col-xs-12 text-primary text_bet textBet">
													
										    			<i class="fa fa-quote-left" aria-hidden="true"></i>
										    			<?php
							    							 		
							    							 		echo $donneesBets['description']
										    			?>
										    			<i class="fa fa-quote-right" aria-hidden="true"></i>
										    	</div>
										    	
									    	</div>
									    </div>	
							    	<?php	
							    }}
							    $groupeBets->closeCursor();
						?>
	        			</div>

	        			
	        		</div>

	        	</div>


	        	
<!--**************************************************************** Pager bets **********************************************************************-->

        	
				<nav>
				<ul class="pager">
					<?php 
						if ($numPage != 0)
						{
					?> 
						<li class="pager-prev"><a href="groupe.php?profil=<?php echo $profilInit?>&id=<?php echo $_GET['id']?>&page=<?php echo $numPage-1;?>"><strong>previous</strong></a></li>
					<?php
						}
						$nbrParis = 0;
						$groupeBets->execute(array($_GET['id']));
					    while($donnees = $groupeBets->fetch())
					    {
					    	$nbrParis += 1;
					    }
						if ($nbrParis-4*$numPage > 4)
						{
					?> 

						<li class="pager-next"><a href="groupe.php?profil=<?php echo $profilInit?>&id=<?php echo $_GET['id']?>&page=<?php echo $numPage+1;?>"><strong>next</strong></a></li>

					<?php
						}
					?>					
				</ul>
				</nav>

<!--**************************************************************** Leave group **********************************************************************-->

<!-- Only if this is not the public group -->

<?php
if( strcmp( htmlspecialchars($_GET['id']) , '1' ) != 0 ) {
?>



	<div class="col-md-12 col-xs-12 bg">
		<!-- Trigger the modal with a button -->
		<button type="button" name='idgroup' value= '<?php echo $_GET['id']?>' class="btn btn-primary btn-lg btn-block shadow" data-toggle="modal" data-target="#myModal">
											<i class="fa fa-sign-out" aria-hidden="true" ></i>
												Leave group
											</button>


		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Are you sure you want to leave this group?</h4>
		      </div>
		      <div class="modal-body" style="text-align:center;">
		        <p>Warning! Your group score and all your votes in this group will be deleted.</p>
			        <form action = "leave_group.php" method="get" >
						<button type="submit" name='idgroup' value= '<?php echo $_GET['id']?>' class="btn btn-warning">
							Yes
						</button>
					</form>
		      </div>
		      
		    </div>

		  </div>
		</div>
		</br>
	</div>
<?
}
?>

       		
        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</div>
</body>
</html>
