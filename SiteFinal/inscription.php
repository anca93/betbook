<!DOCTYPE html>

<html>

    <head>

    <meta charset="-8">
    <title>BetBook|The social betting</title>   
    <link rel="stylesheet" href="utils/style_inscription.css" /> 
    <link rel="icon" href="utils/image/logo.ico" />
        <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">


    </head>

    <body>


        <!-- Bandeau--> 


    <div class="row">
        <header>
            <div class="col-md-2 hidden-sm-down">

            <img src="utils/image/logo.png" width="130" height="80" alt="" title="" style="float:left;"></img>

            </div>




            <!--*************************Connexion********************************-->

                <form action="controleur_login.php" method="post">

                   
                <div class="logbox">
                <div class="col-md-offset-1 col-md-2 col-xs-12">

                <input type="text" placeholder="Username" name="pseudo_login" />
                </div>

                <div class="col-md-2 col-xs-12">

                <input type="password" placeholder="Password"name="mdp_login" />
                </div>

                <div class="col-md-2 col-xs-12">


                <button class="btn btn-primary" type="submit">Login</button> 
                </div>  
            
                
                </form>
               

                <div class="col-md-2 col-xs-12">
                     <?php
                if (isset($_GET['connexion']) AND ($_GET['connexion']==0 )){
                    
                   echo '<div class="row">
                   <div class="col-md-4 col-xs-12">
                        <div class="blockline">
                        <span class=incorrect>Failed...</span>
                        </div>
                         </div>
                         </div>
                     ';   
                }
                ?>

                
                </div>
            </div>
            

            
                </div>

                
        </header>
    


        <!-- Corps--> 
        <div id="corps">

            <!-- Gauche--> 
            <div class="row">
                <div class="down">
 
            <div class="hidden-sm-down col-md-8">
                <div class="sentence">
                 <i> Make bets, not war !</i>
                    </br>
                    <strong><font size=13 >BetBook</font></strong>, the first betting social network.
            
                </div>

            </div>
            

            <!-- Droite--> 


            <div class="col-md-4 col-xs-12">
                <div class="corps_droite">
                <strong><span class="titre">Join us !</strong>
            </br>
            <div class="obligatoire">
                    Fields with * may not be filled.
                </div>
            </br>

                <form action="controleur.php" method="post" onSubmit="return validation(this)"  >


                    <!--Alignement des boutons-->

                    <!--******************Inscription**********************************-->
                <?php

                    
                if (isset($_GET['pseudo_utilise']) AND ($_GET['pseudo_utilise']==0 )){
                    
                   echo '
                        <span class=incorrect>Already used</span>
                     ';   
                }
                if (isset($_GET['pseudo_invalide']) AND ($_GET['pseudo_invalide']==0 )){
                    
                   echo '
                        <span class=incorrect>Beetween 4 and 15 letter or number</span>
                     ';   
                }

                        ?>
                        
                       
                        
                        <span class="aligne" ><input type="text" name="pseudo" placeholder="Username" /></span>
                    </br>

                    <?php
                         if (isset($_GET['mdp_invalide']) AND ($_GET['mdp_invalide']==0 )){
                    
                   echo '
                        <span class=incorrect>Between 4 and 20 letter or number</span>
                     ';   
                }

                        ?>

                        <span class="aligne" ><input type="password" name="mdp" id="mdp"  placeholder="Password"/></span>

                        
                        
                        <span class="aligne" ><input type="password" name="mdp2" id="mdp2" placeholder="Re-type your password"/></span>
                        
                    </br>

                     <?php
                        if (isset($_GET['email_utilise']) AND ($_GET['email_utilise']==0 )){
                    
                   echo '
                        <span class=incorrect>Already used</span>
                     ';   
                }
                         if (isset($_GET['mail_invalide']) AND ($_GET['mail_invalide']==0 )){
                    
                   echo '
                        <span class=incorrect>Do not match</span>
                     ';   
                }

                        ?>
                        
                        <span class="aligne" ><input type="text" name="email" placeholder="Mail" /></span>
                        <span class="aligne" ><input type="text" name="email2" placeholder="Re-type your mail" /></span>
                       
                    </br>
                         <?php

                         if (isset($_GET['date_invalide']) AND ($_GET['date_invalide']==0 )){
                    
                   echo '
                        <span class=incorrect>yyyy-mm-dd </span>
                     ';   
                }

                        ?>
                        <span class="aligne" >
                            <input type="text" name="birth"  placeholder="Birthday (aaaa-mm-dd)" />
                        </span>
                   
                    </br>
                        <span class="aligne" ><input type="text" name="ville" placeholder="City *" /></span>
                    </br>
                        
                        <span class="aligne" ><input type="text" name="entreprise" placeholder="Firm or University *" /></span>
                        </br>
                       <span class="aligne" ><button type="submit" class="btn btn-primary" onClick="AfficherMasquer('divcoucou')">Sign in</button>
                        

                    

                </form>

                

            </div> <!-- fin droite -->


            </div> <!-- fin down -->

            </div> <!-- fin droite -->

            </div> <!-- fin row -->

            <div class="row">
                <div class="hidden-sm-down col-md-12">
                 <footer >

                    <div class="text_info">
                  <div class="col-xs-offset-4 col-xs-1">

                  <a type="button" class="btn btn-primary" onClick="window.location='mailto:contact@betbook.esy.es?subject=Question about BetBook';">contact us</a>
                  
                  </div>
                  <div class="col-xs-offset-1 col-xs-1">
                  <a type="button" class="btn btn-primary" href="about.html">about this project</a>
                  </div>


                </div>
                </footer> 
            </div>
        </div>




                <!--*************champs incorrects************-->

                

                <script language="JavaScript">
                    function validation(f) {
                    
                       if (f.mdp.value != f.mdp2.value) {
                        alert('Error: passwords are not the same!');
                        f.mdp.focus();
                        return false;
                        }
                      else if ((f.mdp.value == f.mdp2.value)&&(f.email.value==f.email2.value)) {
                        return true;
                        }
                      else if(f.email.value!=f.email2.value) {
                        alert('Error: email are not the same');
                        f.mdp.focus();
                        return false;
                        }

                      }

                        // function Afficher(arg,arg2,arg3,arg4){
                        //   divInfo = document.getElementById(arg);
                        //   divInfo2=document.getElementById(arg2);
                        //   tape=document.getElementById(arg3);
                        //   retape=document.getElementById(arg4);
                        //   if(tape.value != retape.value){
                        //     divInfo.style.display='none';
                        //     divInfo2.style.display='block';
                        //     alert("caca");
                        // }else{
                        //     divInfo2.style.display='none';
                        //     divInfo.style.display='block';


                        // }




                        // }
                    </script>   
             
    </body>

</html>