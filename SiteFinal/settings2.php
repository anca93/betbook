<?php
    

    if(session_start()==0 || $_SESSION['idmembre'] ==0)
  {
    header('Location: inscription.php'); 
    exit();
  }
    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    $pseudo = $_SESSION['pseudo'];
    $mail = $_SESSION['mail'];
    $ville = $_SESSION['ville'];
    $entreprise = $_SESSION['entreprise'];
    $birthday = $_SESSION['birthday'];
    $score = $_SESSION['score'];
    
    //include des request sql et la bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //on prend l'image de l'utilisateur de la bdd

    $getPicture->execute(array($idMembre));

    $donnees = $getPicture->fetch();
    
    $image = $donnees['picture'];

  
    //**********************************************************************************//


?>
<script>

</script>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>BetBook|The social betting</title> 
    <link rel="icon" href="utils/image/logo.ico" />

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

</head>
<body>
<div class="corps">
 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">   
          <div class="col-md-12 col-xs-12 bg">
          <div class="container">
            <div class="row">

              <h1 class="titre">Edit profile</h1>

            <div class="row ">
                <div class="col-md-6 col-xs-12">
                    <div class="col-xs-12" style="text-align: center; border-radius: 20px;">
                        <?php
                          if (isset($_GET['image_change']) AND ($_GET['image_change']==1 ))
                          {
                              $image = $_SESSION['image'];
                          }
                        ?>
                        <img src="utils/image/<?php echo $image?>"  style="margin-top: 10px; margin-bottom: 10px; border-radius: 10px;"
                         width="210" height="200" title="" style="float:center;"></img>
                         <?php
                         if( !empty($message) ) 
                         {
                          echo '<p>',"\n";
                          echo "\t\t<strong>", htmlspecialchars($message) ,"</strong>\n";
                          echo "\t</p>\n\n";
                        }
                        ?>
                    </div>
                     <div class="col-xs-12 " style="text-align: center;">

                            <button class="buttons" onClick="AfficherMasquer('divacacher')">
                              <span class="add"></span>
                            </button>
                      
                        <div id="divacacher" style="display:none; border-radius: 10; background-color:  #F0F0F0  " >
                           <form enctype="multipart/form-data" action="settings_upload.php" method="post">
                            <fieldset>
                              <legend>Choose a picture !</legend>
                                <label for="fichier_a_uploader" title="Recherchez le fichier à uploader !">Send this picture :</label>
                                <input name="fichier" style="max-width: 100% "type="file" id="fichier_a_uploader" />
                                <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                            </fieldset>
                          </form>
                          </div>
                    </div>
                </div>



  <!--*********************************************************Picture**********************************************************************-->

               
<div class="col-md-6 col-xs-12">

<p><span class="title">Personal Informations:</span><p>

<!--***********************************************************Pseudo**********************************************************************-->


<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-user fa-fw bg2 ellipsis"></i>
    <a><button  class="buttons droite"  onClick="AfficherMasquer('divpseudo')"><span class="add"></span></button> </a>

    <?php
      //echo '<span class="champs">'.$_SESSION['pseudo'].'</span>';
      echo '<span class="champs">'.$pseudo.'</span>';
    ?>
  </a>
</ul>
<?php
if (isset($_GET['pseudo_utilise']) AND ($_GET['pseudo_utilise']==0 )){

 echo '<p><span class=invalide>This login is already taken..<span></p>';   
}

if (isset($_GET['new_pseudo']) AND ($_GET['new_pseudo']==0 )){

 echo '<p><span class=invalide>Beetween 4 and 15 letter or number<span></p>';   
}

?>
<div id="divpseudo" style="display:none;">
 <form action="settings_update.php" method="post">
  <input type="text" name="new_pseudo" placeholder="New pseudo" />
  <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
</form>

</div>
<!--***********************************************************Mail**********************************************************************-->

<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-envelope-o fa-fw bg2 ellipsis"></i>
    <a><button  class="buttons droite"  onClick="AfficherMasquer('divmail')"><span class="add"></span></button> </a>

    <?php
        //echo '<span class="champs">'.$_SESSION['mail'].'</span>';
        echo '<span class="champs">'.$mail.'</span>';
    ?>
  </a>
</ul>
<?php
if (isset($_GET['email_utilise']) AND ($_GET['email_utilise']==0 )){

 echo '<p><span class=invalide>This email is already taken..<span></p>';   
}

if (isset($_GET['new_mail']) AND ($_GET['new_mail']==0 )){

 echo '<p><span class=invalide>Email incorrect<span></p>';   
}

?>

<div id="divmail" style="display:none;">
 <form action="settings_update.php" method="post">
  <input type="text" name="new_mail" placeholder="Email" />
  <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
</form>

</div>
<!--***********************************************************Password****************************************************************-->

<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-key fa-fw bg2 ellipsis"></i>
    <a><button  class="buttons droite"  onClick="AfficherMasquer('divpassword')"><span class="add"></span></button> </a>

    <?php
    echo '<span class="champs">Password</span>';
    ?>
  </a>
</ul>
<?php

if (isset($_GET['new_password']) AND ($_GET['new_password']==0 )){

 echo '<p><span class=invalide>Beetween 4 and 15 letter or number<span></p>';   
}

if (isset($_GET['same_password']) AND ($_GET['same_password']==0 )){

 echo '<p><span class=invalide>Password incorrect<span></p>';   
}

if (isset($_GET['password_valid']) AND ($_GET['password_valid']==0 )){

 echo '<p><span class=invalide>Password changed<span></p>';   
}



?>

<div id="divpassword" style="display:none;">
 <form action="settings_update.php" method="post">
  <input type="password" name="old_password" placeholder="Old Password"  />
  <input type="password" name="new_password" placeholder="New Password"  />

  <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
</form>

</div>
<!--*********************************************************Birthday**********************************************************************-->

<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-birthday-cake fa-fw bg2 ellipsis"></i>
    <a><button  class="buttons droite"  onClick="AfficherMasquer('divbirthday')"><span class="add"></span></button> </a>

    <?php
        //echo '<span class="champs">'.$_SESSION['birthday'].'</span>';
        echo '<span class="champs">'.$birthday.'</span>';
    ?>
  </a>
</ul>

<?php

if (isset($_GET['new_birthday']) AND ($_GET['new_birthday']==0 )){

 echo '<p><span class=invalide>yyyy-mm-dd<span></p>';   
}

?>
<div id="divbirthday" style="display:none;">
 <form action="settings_update.php" method="post">
  <input type="text" name="new_birthday" placeholder="aaaa-mm-jj" />
  <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
</form>

</div>
<!--*********************************************************City**********************************************************************-->

<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-building fa-fw bg2 ellipsis"></i>
    <a><button  class="buttons droite"  onClick="AfficherMasquer('divcity')"><span class="add"></span></button> </a>

    <?php
        //echo '<span class="champs">'.$_SESSION['ville'].'</span>';
        echo '<span class="champs">'.$ville.'</span>';
    ?>
  </a>
</ul>
<?php

if (isset($_GET['new_city']) AND ($_GET['new_city']==0 )){

 echo '<p><span class=invalide>This city does not exist !<span></p>';   
}

?>

<div id="divcity" style="display:none;">
 <form action="settings_update.php" method="post">
  <input type="text" name="new_city" placeholder="City" />
  <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
</form>

</div>
<!--***********************************************************University******************************************************************-->

<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-university fa-fw bg2 ellipsis"></i>
    <a><button  class="buttons droite"  onClick="AfficherMasquer('divcompany')"><span class="add"></span></button> </a>

    <?php
        //echo '<span class="champs">'.$_SESSION['entreprise'].'</span>';
        echo '<span class="champs">'.$entreprise.'</span>';
    ?>
  </a>
</ul>

<?php

if (isset($_GET['new_university']) AND ($_GET['new_university']==0 )){

 echo '<p><span class=invalide>This school is very bad !<span></p>';   
}

?>

<div id="divcompany" style="display:none;">
 <form action="settings_update.php" method="post">
  <input type="text" name="new_university" placeholder="University/Company" />
  <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
</form>

</div>
<!--***********************************************************Score**********************************************************************-->

<ul class="nav-item active">
  <a class="nav-link"><i class="fa fa-trophy fa-fw bg2 ellipsis"></i>

    <?php
        //echo '<span class="champs">'.$_SESSION['score'].' Points</span>';
        echo '<span class="champs">'.$score.' Points</span>';
    ?>
  </a>
</ul>                    
</div>

</div>



</div>
</div>
</div>

</div>


        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
<!-- Debut du formulaire -->
<script type="text/javascript">
    /* Voici la fonction javascript qui change la propriété "display"
    pour afficher ou non le div selon que ce soit "none" ou "block". */

    function AfficherMasquer(arg)
    {
      divInfo = document.getElementById(arg);

      if (divInfo.style.display == 'none')
        divInfo.style.display = 'block';
      else
        divInfo.style.display = 'none';

    }
     function extractUrlParams () {
      var t = location.search.substring(1).split('&');
      if(t=="image_change=1")
      alert("Invalid picture: must be under than: 10ko size, 800px height and width");    
    }


    extractUrlParams(); 
 



  </script>
  </div>
</body>
</html>
