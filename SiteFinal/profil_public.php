<?php

  session_start();

  $pseudo = $_GET['pseudo'];

  $idMembre = $_SESSION['idmembre'];
  
  //include des request sql et connection bdd
  //**********************************************************************************//
  include 'utils/sqlrequest.php';
  //**********************************************************************************//

  
  $getMembre->execute(array(
    'pseudo' => $pseudo));

  $resultat = $getMembre->fetch();
  $picture=$resultat['picture'];
  $birthday=$resultat['birthday'];
  $ville=$resultat['ville'];
  $mail=$resultat['mail'];
  $entreprise=$resultat['entreprise'];
  $score=$resultat['score'];
  
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <title>BetBook|The social betting</title> 
      <link rel="icon" href="utils/image/logo.ico" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">   
           
            
          <div class="col-md-12 col-xs-12 bg">
          <div class="container">
            <div class="row">

              <h1 class="titre"><?php echo $pseudo;?></h1>

            <div class="row ">
              <div class="col-md-5 col-xs-12">

<!--*********************************************************Picture****************************************************-->

                 </br>
                 <img src="utils/image/<?php echo $picture?>" width="210" height="200" alt="" title="" ></img>

              </div>

                  <div class="col-md-7 col-xs-12">
                      <p><span class="title">Personal Informations:</span><p>

<!--***********************************************************Pseudo**********************************************-->
                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-user fa-fw bg2 ellipsis"></i>
                          
                          <?php
                          echo '<span class="champs">'.$pseudo.'</span>';
                          ?>
                        </a>
                      </ul>
                      
<!--***********************************************************Mail***************************************************-->

                     <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-envelope-o fa-fw bg2 ellipsis"></i>
                          
                          
                          <?php
                          echo '<span class="champs">'.$mail.'</span>';
                          ?>
                        </a>
                      </ul>
                      

<!--**************************************Birthday**********************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-birthday-cake fa-fw bg2 ellipsis"></i>
                          
                          <?php
                          echo '<span class="champs">'.$birthday.'</span>';
                          ?>
                        </a>
                      </ul>

                      
<!--*********************************************************City**********************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-building fa-fw bg2 ellipsis"></i>
                          
                          
                          <?php
                          echo '<span class="champs">'.$ville.'</span>';
                          ?>
                        </a>
                      </ul>
                     
<!--**************************************University******************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-university fa-fw bg2 ellipsis"></i>
                          
                          
                          <?php
                          echo '<span class="champs">'.$entreprise.'</span>';
                          ?>
                        </a>
                      </ul>

                     
<!--***********************************************************Score*****************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-trophy fa-fw bg2 ellipsis"></i>
                          
                          <?php
                          echo '<span class="champs">'.$score.' Points</span>';
                          ?>
                        </a>
                      </ul>                    
                  </div>

</div>


</div>
</div>
</div>





        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</body>
</html>
