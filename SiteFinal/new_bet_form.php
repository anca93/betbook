<?php
    
    
    if(session_start()==0 || $_SESSION['idmembre'] ==0)
    {
        header('Location: inscription.php'); 
        exit();
    }
    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    $pseudo = $_SESSION['pseudo'];
    //**********************************************************************************//
    

    //include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //**********************************************************************************//

    /* Récupération de l'id du groupe où poster */
    if ( ! isset($_GET['idgroup'])) {
        header('redirect.php?error=1');       
    }

    $idGroup = $_GET['idgroup']; 
        
    //verifie l'acces au groupe
    //********************************************************************************//           
        
    $present=0;
    $header->execute();
    while($donneesGroupe = $header->fetch()){
        if( $idGroup == $donneesGroupe['idgroupe']){
            $present = 1;
        }
    }
    
    $header->closeCursor();     
    
    if( ! $present){
        header('Location: redirect.php?error=2');
        
    }



//verifie qu'il n'y a pas plus de 5 créations dans la journée
    //********************************************************************************//           
    $creationmax->execute(array('idmembre'=>$_SESSION['idmembre']));
    $donneesmax=$creationmax->fetch();
    

    if($donneesmax['max']>=5){

        header('Location: redirect.php?error=3');
     

    }
    $creationmax->closeCursor();

    /* Tableau  qui contiendra les erreurs de saisies */
    $errors = array();

    if ( isset($_POST["submit"]) ) {

        if ( strlen(   $_POST["question"]   ) <10){
            $errors["question"][] = "Non valid question (less than 10 characters)." ;
        }
        if ( strlen(   $_POST["answer1"]   ) <1){
            $errors["answer1"][] = "Non valid answer 1 (less than 1 character)." ;
        }
        if ( strlen(   $_POST["answer2"]   ) <1){
            $errors["answer1"][] = "Non valid answer 2 (less than 1 character)." ;
        }
        if ( (strlen(   $_POST["answer3"]   ) <1)&&(strlen(   $_POST["answer3"]   ) >0)){
            $errors["answer1"][] = "Non valid answer 3 (less than 1 character1)." ;
        }
        if ( (strlen(   $_POST["answer4"]   ) <1)&&(strlen(   $_POST["answer4"]   ) >0)){
            $errors["answer1"][] = "Non valid answer 4 (less than 1 character)." ;
        }
        if ( (strlen(   $_POST["answer5"]   ) <1)&&(strlen(   $_POST["answer5"]   ) >0)){
            $errors["answer1"][] = "Non valid answer 5 (less than 1 character)." ;
        }


        if ( (strlen(   $_POST["day"]   )!=10)||( preg_match(" #^([0-3][0-9]})(/)([0-9]{2,2})(/)([0-3]{2,2})$# ", $_POST["day"] ))){
            $errors["date"][] = "Non valid date (must be dd/mm/yyyy)." ;
        }

        //Vérif date 1 heure à l'avance 1h minimum.

        else if (    mktime( ($_POST["hour"][0].$_POST["hour"][1])+1,$_POST["hour"][3].$_POST["hour"][4],0, $_POST["day"][3].$_POST["day"][4] , $_POST["day"][0].$_POST["day"][1] , $_POST["day"][6].$_POST["day"][7].$_POST["day"][8].$_POST["day"][9] ) 
            < time() ){
            $errors["date"][] = "Non valid date (must be in the futuuuure)." ;
        }

        
        if (count($errors) == 0) {
            // Pas d'erreurs de saisies
            include("bet_created.php");
            die();
        }
    }


    
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>BetBook|The social betting</title> 
      <link rel="icon" href="utils/image/logo.ico" />

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">
    <!-- Error -->
    <link rel="stylesheet" href="bootstrap/css/error.css">
    <!-- Calendar -->
    <link rel="stylesheet" media="screen" type="text/css" title="Design" href="bootstrap/css/calendar.css" />
        
    <title>BetBook : New Bet</title>

</head>
<body>

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">  

         <div class="col-md-12 col-xs-12 bg"> 

            <div class="container">

                <div class="row">


                        <h1 class="titre"> New Bet </h1>

                    </div>
        <h4> ... and new chance to master the race !</h4>
        <form method="post"  >              <!-- action="bet_form_target.php" -->


            <i>Please fill out this form to make that bet live.</i>
        </br>
    </br>


<?php
    //Le champ caché contenant l'id du groupe ?>

    <input type="hidden" name="idGroup" value="<?php echo htmlspecialchars($idGroup); ?>" />

    <?php
// Si des erreurs ont été trouvées, les afficher sous forme de liste
    if (count($errors) > 0) {
        echo "<div class=\"error\">";
        echo "<ul>";
        foreach ($errors as $champEnErreur => $erreursDuChamp) {
            foreach ($erreursDuChamp as $erreur) {
                echo "<li>".$erreur."</li>";
            }
        }
        echo "</ul>";
        echo "</div>";
    }
    ?>


</br>


<div class="row">

    <div class="col-sm-12">

        Ask a question
        <input type="text" name="question" placeholder="?" class="form-control input-lg" />
    </br>

    Define the possible answers (up to 5)
    <div class="input-group">
        <span class="input-group-addon">1</span> 
        <input type="text" name="answer1" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">2</span>
        <input type="text" name="answer2" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">3</span> 
        <input type="text" name="answer3" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">4</span> 
        <input type="text" name="answer4" class="form-control">      
    </div>
    <div class="input-group">
        <span class="input-group-addon">5</span> 
        <input type="text" name="answer5" class="form-control">      
    </div>

</br>



Add a few details

<textarea name="details" rows="2" class="form-control input-sm"></textarea>
</br>


<div class="row">
    <div class="col-xs-offset-1 col-xs-4">
        Locate this bet ? 
        <div class="input-group">
            <span class="input-group-addon">
                <input type="checkbox" name="city_check">
            </span>
            <input type="text" name="city" placeholder="City" class="form-control">
        </div>
        <br/>
        Tag a firm ? 
        <div class="input-group">
            <span class="input-group-addon">
                <input type="checkbox" name="firm_check">
            </span>
            <input type="text" name="firm" placeholder="Company" class="form-control">
        </div>
        <br/>

    </div>
    <div class="col-xs-offset-2 col-xs-4">


        End of bet: </br>
        <input type="text" name="day" id="day" class="calendrier" size="8" />
    </br> at </br>
    <select name="hour">
        <?php

        $end = new Datetime('22:00');
        for ($t =  new Datetime('08:00'); $t <= $end; $t->modify('+30 minutes')) {
           $value = $t->format('H:i');
           echo '<option value="' . $value . '">' . $value . '</option>';
       }?>
   </select>

</br></br>

</div>
</div> 


</br>
</div>


<strong></br><h4><center>
    Ready to rule the world ?
</br>
<button type="submit" name="submit" class="btn btn-primary btn-lg"> Submit </button>
</strong></h4></center>
</br>               
           
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="bootstrap/js/calendar.js"></script>  
</body>
</html>
