<?php

    

    if(session_start()==0 || $_SESSION['idmembre'] ==0)
    {
        header('Location: inscription.php'); 
        exit();
    }
    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et la bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>
<div class="corps">
 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">   

            

            <div class="row">
                        
                            <div class="col-md-12 col-xs-12">

                            <h3_noline>You do not have access to this group!</h3_noline>

                                <form action = "join_group.php" method="get" >
                                    <button type="submit" name='idgroup' value= '<?php echo $_GET['id']?>' class="btn btn-primary btn-lg btn-block shadow">
                                        Join group !
                                    </button>
                                </form>
                            </div>





        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</div>
</body>
</html>
