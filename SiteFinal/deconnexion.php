<?php
    session_start();
    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

	session_destroy();

    require_once('inscription.php');
    exit();

?>