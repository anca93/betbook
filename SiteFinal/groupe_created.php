<?php
 
    //variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idadmin_groupe =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //**********************************************************************************//

    $description_groupe = htmlspecialchars($_POST["details"]);
    $nom_groupe = htmlspecialchars($_POST["name"]);

    $friends_string = htmlspecialchars($_POST["friend"]);
    $friends_array = preg_split("/[\s;]+/", $friends_string);


try
{

    //Insertion roupe dans la base des donnees
    $req = $bdd->prepare('INSERT INTO Groupe(nom_groupe, creation_date_groupe, idadmin_groupe, description_groupe) VALUES(:nom_groupe, CURDATE(), :idadmin_groupe, :description_groupe)');

    $req->execute(array('nom_groupe' => $nom_groupe , 'idadmin_groupe' => $idadmin_groupe, 'description_groupe' => $description_groupe));


    //Recuperation du id du groupe cree
    $idgroupe = $bdd->lastInsertId();
    
    //Insertion admin - groupe dans la table jonction
    $req = $bdd->prepare('INSERT INTO Jonction(idmembre, idgroupe, score_groupe) VALUES(:idmembre, :idgroupe, :score_groupe)');

    $req->execute(array('idmembre' => $idadmin_groupe, 'idgroupe' => $idgroupe, 'score_groupe' => 0));

    //Ajout du chaque ami + groupe dans la table Jonction

    foreach ($friends_array as $value) {
        if($value != null)
        {
            //Recuperation du id du membre en sachant son pseudo

            $req = $bdd->prepare("SELECT idmembre FROM Membre WHERE pseudo = :pseudo");

            $req->execute(array(':pseudo' => $value));

            $idmembre_row = $req->fetch(PDO::FETCH_ASSOC);

            $idmembre = $idmembre_row['idmembre'];

            if($idmembre != 0)
            {

                $req = $bdd->prepare('INSERT INTO Jonction(idmembre, idgroupe, score_groupe) VALUES(:idmembre, :idgroupe, :score_groupe)');

                $req->execute(array('idmembre' => $idmembre, 'idgroupe' => $idgroupe,  'score_groupe' => 0));
            }
        }
    }
    

    
} 
catch(Exception $e)
{   
    die('Erreur : '.$e->getMessage());
}


header('Location: groupe.php?id='.$idgroupe);

exit();


/*
    
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">

            
            <h1 class="titre"> Groupe succefully created! </h1>

            <i>Details: </i>

            </br></br>

            <div class="row">

                    <div class="col-sm-12">

                        </br></br>

                        Id: <?php echo $idgroupe; ?>

                        </br></br>

                        Name: <?php echo $nom_groupe; ?>

                        </br></br>
                        

                        Description:

                        </br>

                        <?php echo $description_groupe; ?>

                        </br></br>

                        Friends added:

                        </br>

                        <?php

                            foreach ($friends_array as $value) {
                                echo $value;
                            }

                        ?>

                        </br>

                    </br>

                </div>
        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</body>
</html>

*/