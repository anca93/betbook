<?php
   

   if(session_start()==0 || $_SESSION['idmembre'] ==0)
    {
        header('Location: inscription.php'); 
        exit();
    }
    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et la bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    if(isset($_GET['search']))
    {
         $_GET['search'] = htmlspecialchars($_GET['search']);


         if( $_GET['search'] == '')
         {
            header('Location: main.php'); 
         }
    }
    else
    {
        header('Location: main.php');
    }

    //**********************************************************************************//

    $searchTerm = $_GET['search'];

    //recherche utilisateurs    
    $searchUsers->execute(array('searchTerm' => $searchTerm . '%'));

    $nb_users = $searchUsers->rowCount();

    if($nb_users > 0)
    {
        while($donnees = $searchUsers->fetch())
        {
            $users[] = $donnees['pseudo'];
        }
    }

    $searchUsers->closeCursor();

    //recherche groupes
    $searchGroups->execute(array('searchTerm' => $searchTerm . '%'));

    $nb_groups = $searchGroups->rowCount();

    if($nb_groups > 0)
    {
        while($donnees = $searchGroups->fetch())
        {
            $groups[$donnees['idgroupe']] = $donnees['nom_groupe'];

        }
    }

    $searchGroups->closeCursor();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>
<div class="corps">

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">

            <div class="col-md-12 col-xs-12 bg">
                    
                    <div class="container">

                        <div class="row">

                            <h1 class="titre"> Searched term: <?php echo $searchTerm?></h1> 

                            <form method="post">




                <div class="row">

                    <div class="col-sm-12">

                        <h4>Users</h4>
                        <div class="form-group">
                            <i><?php 

                            if($nb_users > 0)
                            {
                                foreach ($users as $value) {
                                    ?>

                                    <a class="nav-link" href="profil_public.php?pseudo=<? echo $value;?>"><? echo $value;?></a>
                                    </br>

                                    <?
                                }
                            }
                            else echo 'No users found';

                            ?></i>
                        </div>

                        <h4>Groups</h4>
                        <div class="form-group">
                            <i><?php

                            if($nb_groups > 0)
                            {
                                foreach ($groups as $key => $value) {
                                    ?>

                                    <a class="nav-link" href="groupe.php?id=<? echo $key;?>"><? echo $value;?></a>
                                    </br>

                                    <?
                                }
                            }
                            else echo 'No groups found';

                            ?></i>
                        </div>

                    </br>

                </div>




        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</div>
</body>

</html>
