<?php

	if(session_start()==0 || $_SESSION['idmembre'] ==0)
	{
		header('Location: inscription.php'); 
		exit();
	}
	//variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//


    //include des request sql
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//


    if(isset($_GET['id']))
    {
    	$_GET['id'] = (int)htmlspecialchars($_GET['id']);

    	if($_GET['id'] <= 0)
    	{
    		header('Location: main.php'); 
    	}
    }

    $nbrParis = 0;

    include("utils/sqlrequest.php");
    
    //verifie l'acces au paris
   	//**********************************************************************************//   
   	$present = 0;
   	
   	$getParis->execute(array($_GET['id']));
    $donneesParis = $getParis->fetch();

    $header->execute();

    while($donneesHeader = $header->fetch())
	{
        if($donneesHeader['idgroupe'] == $donneesParis['idgroupe'])
    	{
    		$present = 1;
	    }
    }
   		
    $getParis->closeCursor();   	
    $header->closeCursor();

   	if($present != 1)
   	{
   		header('Location: main.php'); 
   	}

   	$getParis->execute(array($_GET['id']));

	$donneesParis = $getParis->fetch();

	$end_date = mktime( $donneesParis['end_date'][11].$donneesParis['end_date'][12], 
				$donneesParis['end_date'][14].$donneesParis['end_date'][15],
				$donneesParis['end_date'][17].$donneesParis['end_date'][18],
				$donneesParis['end_date'][5].$donneesParis['end_date'][6],
				$donneesParis['end_date'][8].$donneesParis['end_date'][9],
				$donneesParis['end_date'][0].$donneesParis['end_date'][1].$donneesParis['end_date'][2].$donneesParis['end_date'][3]);

	$getAnswer->execute(array($idMembre, $_GET['id']));
	$donneesAnswer = $getAnswer->fetch();

	$present = 0;

	$getVote = $bdd->prepare('SELECT * FROM Vote WHERE idmembre = ?');
	$getVote->execute(array($idMembre));

	while($donneesVote = $getVote->fetch())
    {
   		if($_GET['id'] == $donneesVote['idparis'])
    	{
    		$present = 1;
	    }
    }
	
?>
<!DOCTYPE html>
	<html lang="en">
	<head>
	    <!-- Required meta tags always come first -->
	    <meta charset="utf-8">
	    <title>BetBook|The social betting</title> 
	      <link rel="icon" href="utils/image/logo.ico" /> 
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	    <meta http-equiv="x-ua-compatible" content="ie=edge">

	    <!-- Bootstap 4 -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
	    <!-- perso -->
	    <link rel="stylesheet" href="bootstrap/css/perso.css">
	    <!-- pour les icons -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	    <!-- Bootstrap Vertical Nav -->
	    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

	</head>
	<body>
<div class="corps">

	 <?php include("utils/navbar.php"); ?>
	
	<div class="container">
	    <div class="row">

	        <?php include("utils/leftmenu.php"); ?>

	        <div class="col-md-9 top">
	        	<div class="container">
		        	<div class="row">
							<div class="col-md-12 col-xs-12 bg">
			       			<h2><strong><?php echo $donneesParis['nom_paris'];?></strong></h2>

			       			<h3><?php echo $donneesParis['description'];?></h3>

<!--************************ Affichage different des reponses en fonction de l'etat du paris *******************************************-->

			       			<?php


			       				if( $end_date < time() and $idMembre == $donneesParis['idadmin_paris'] and $donneesParis['good_answer']==0)
								{
									//quand on est admin et il faut choisir la bonne reponse apres expiration
									include('utils/admin_choose_answer.php');
								}
								else if( $end_date < time() )
								{
									//apres expiration
									include('utils/bet_expired.php');
								}
								else if($present == 1 and $end_date > time() )
							   	{   
							   		//reponse choisi mais bet encore actif
									include('utils/answer_selected.php');
								}
								else
								{
									//bet actif, sans reponse
									include('utils/choose_answer.php');
								}

			       			?>

<!--*********************************************** Info pari ******************************************************************************************-->

					        <div class="row">
				        		<div class="col-lg-5 col-md-12">
				        			<div class="container">
							    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
								    		<div class="row">
								    			<div class="col-md-12 col-xs-12 bg-primary center_text">
										    			<p >
										    			<i class="fa fa-info-circle fa-3x" aria-hidden="true"></i>
												    	</p>
										    	</div>
										    	<div class="col-md-12 col-xs-12 text-primary">
										    		<li>
														Posted by :
														<a href="main.php">
											    			<?php 
											    				$getNameUser->execute(array($donneesParis['idadmin_paris']));
											    				$name = $getNameUser->fetch();
											    				echo $name['pseudo'];
											    			?>

											    		</a>
											    	</li>
										    	</div>

										    	<?php 
										    				$getGroupFromBet->execute(array($_GET['id']));
										    				$name = $getGroupFromBet->fetch();
										    	?>

										    	<div class="col-md-12 col-xs-12 text-primary">
													<li>
														<a href="groupe.php?id=<?php echo $name['idgroupe']?>">
											    			<i class="fa fa-users fa-fw "></i>
											    			<?php 
											    				echo $name['nom_groupe'];
											    			?>
											    		</a>
											    	</li>
										    	</div>
										    	<div class="col-md-12 col-xs-12 text-primary">
										    		<li
										    			<a>
												    		<i class="fa fa-clock-o fa-fw"></i>
										    				<?php 
										    					$date = date_parse($donneesParis['end_date']);
									    						//echo $date['month'].'/'.$date['day'].'/'.$date['year'];


									    						$currentTime = time();
										    							 		$endTime = mktime(  $date['hour'], 
																									$date['minute'],
																									$date['second'],
																									$date['month'],
																									$date['day'],
																									$date['year']);

										    							 		$remainingTime = $endTime - $currentTime;
										    							 		if($remainingTime>0){
										    							 			$rd = (int)($remainingTime / 86400); //jours
										    							 			$rh = (int)(($remainingTime - ($rd*86400))/3600); //heures
										    							 			if($rd>0){
										    							 				echo $rd.' days | '.$rh.' hours';
										    							 			}else{
										    							 				echo $rh.' hours';
										    							 			}
										    							 		}else{
										    							 			echo '<font color="orange">End date reached !</font>';
										    							 		}



										    				?>
									    				</a>	
									    			</li>
										    	</div>

										    	<?php
										    	if($donneesParis['ville'] != 'NONE')
										    	{
										    	?>		
										    	<div class="col-md-12 col-xs-12 text-primary">
													<li>
										    			<i class="fa fa-building fa-fw "></i>
										    			<?php echo $donneesParis['ville'];?>
										    		</li>
										    	</div>
										    	<?php
										    	}
										    	?>

										    	<?php
										    	if($donneesParis['entreprise'] != 'NONE')
										    	{
										    	?>		
										    	<div class="col-md-12 col-xs-12 text-primary ">
										    		<li>
										    			<i class="fa fa-industry fa-fw"></i>
										    			<?php 
										    			
										    				echo $donneesParis['entreprise'];
										    			?>
										    		</li>
										    	</div>
										    	<?php
										    	}
										    	?>
										    </div>
								    	</div>	
								    </div>
							    </div>
								<div class="col-lg-7 col-md-12">
								
<!--*************************************************** Reponses des autres ************************************************************************************-->

								<div class=" container">
					        		<div class="row">
					        			<div class="col-md-12  col-xs-12">
								    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
									    		<div class="row">
									    			<div class="col-md-12 col-xs-12 bg-primary center_text">
											    		<p >
											    			<i class="fa fa-users fa-3x" aria-hidden="true"></i>
													    	</p>
											    	</div>
											    	<div>
											    		<div class="container text-primary">
												    		<?php
											    			$getUsers->execute(array($_GET['id']));
											    			$count = 0;
											    			while( $users = $getUsers->fetch())
											    			{
											    				$getNameUser->execute(array($users['idmembre']));
											    				$pseudo = $getNameUser->fetch();
											    				$count += 1;
											    				?>
											    				<li>
											    					<a href="profil_public.php?pseudo=<?php echo $pseudo['pseudo']?>"><?php echo $pseudo['pseudo']?></a>
											    						bet on "
											    						<?php 
											    							$getAnswer->execute(array($users['idmembre'], $_GET['id']));
											    							$idAnswer = $getAnswer->fetch();
											    							$getParis->execute(array($_GET['id']));
											    							$donneesParis = $getParis->fetch();
											    							$answer = 'answer';
											    							$answer = 'answer'.$idAnswer['idreponse_vote'];
											    							echo $donneesParis[$answer];
											    						?>"
											    				</li>
											    				<?php
											    				$getNameUser->closeCursor();
											    			}	
											    			$getUsers->closeCursor();

											    			if ($count == 0) 
											    			{
											    				?>
											    					<i> Nobody answered to this bet yet</i>

											    				<?php

											    			}
											    		?>
												    	</div>
											    	</div>
											    </div>
											</div>
									    </div>	
					        		</div>
					        	</div>
				        	</div>
				        </div>
		        	</div>
		        </div>
	        </div>
	    </div>
	</div>

	<!-- jQuery first, then Bootstrap JS. -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
	</div>
	</body>
	</html>