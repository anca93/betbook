<?php

if(session_start()==0 || $_SESSION['idmembre'] ==0)
{
	header('Location: inscription.php'); 
	exit();
}

    //variable à initialiser avec les variable de scession
    //**********************************************************************************//    
$idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//


    //includes des requetes php et connection bdd
    //**********************************************************************************//
include("utils/sqlrequest.php");
    //**********************************************************************************//

    //analyse de la la variable $_GET['id'] du groupe
    //**********************************************************************************//
if(isset($_GET['id']))
{
	$_GET['id'] = (int)htmlspecialchars($_GET['id']);

	if($_GET['id'] <= 0)
	{
		header('Location: main.php'); 
	}
}

    //**********************************************************************************//

    //analyse de la la variable $_GET['page'] pour la pagination des paris
  	//**********************************************************************************//
$nbrParis = 0;
$randomBetsUnlimited->execute();
while($donnees = $randomBetsUnlimited->fetch())
{
	$nbrParis += 1;
}
$randomBetsUnlimited->closeCursor();

if(isset($_GET['page']))
{
	$_GET['page'] == (int)htmlspecialchars($_GET['page']);

	if($_GET['page'] > 0)
	{
		if($_GET['page']*4 <= $nbrParis)
		{
			$numPage = $_GET['page'];
		}
		else
		{
			$numPage = 0;
		}
	}
	else
	{
		$numPage = 0;
	}
}
else
{
	$numPage = 0;
}
$num = 1+4*$numPage;
$betInit = $numPage*4;
  	//**********************************************************************************//

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags always come first -->
	<meta charset="utf-8">

	<title>BetBook|The social betting</title>  
	<link rel="icon" href="utils/image/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Bootstap 4 -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
	<!-- perso -->
	<link rel="stylesheet" href="bootstrap/css/perso.css">
	<!-- pour les icons -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<!-- Bootstrap Vertical Nav -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>
	<div class="corps">

		<?php include("utils/navbar.php"); ?>

		<div class="container">
			<div class="row">
				<?php include("utils/leftmenu.php"); ?>

				<div class="col-md-9 col-md-9 top container">
					<div class="col-md-12 col-xs-12 bg">

	       			<div class="container">

		       		<!--	<div class="row">
		       				<?php
		       				$getGroup->execute(array($_GET['id']));
		       				$group = $getGroup->fetch();
		       				?>
		        			<h1 class="titre"><?php echo "#"; echo  $group['nom_groupe']; ?></h1>
		        		
		        		<div class="row">
		        		
			        		<div class="col-md-12 col-xs-12">

			        			<i> <?php echo  $group['description_groupe'] ?></i>

								<form action = "new_bet_form.php" method="get" >
									<button type="submit" name='idgroup' value= '<?php echo $_GET['id']?>' class="btn btn-primary btn-lg btn-block shadow">
										Post a Bet
									</button>
								</form>
							</div>-->

							<!--**************************************************************** Bets **********************************************************************-->

						</div>
					</br><h4> Bets: </h4>

					<div class="row">

						<?php
						$recupgroup->execute(array('idmembre' => $_SESSION['idmembre']));
						while($donneesgroup = $recupgroup->fetch()){

							$groupeBetsIndex->execute(array('idgroupe'=> $donneesgroup['idgroupe']));
						

						while($donneesBets = $groupeBetsIndex->fetch())
						{
							$end_date = mktime( $donneesBets['end_date'][11].$donneesBets['end_date'][12], 
								$donneesBets['end_date'][14].$donneesBets['end_date'][15],
								$donneesBets['end_date'][17].$donneesBets['end_date'][18],
								$donneesBets['end_date'][5].$donneesBets['end_date'][6],
								$donneesBets['end_date'][8].$donneesBets['end_date'][9],
								$donneesBets['end_date'][0].$donneesBets['end_date'][1].$donneesBets['end_date'][2].$donneesBets['end_date'][3]);

							    	// si end_date du paris est plus vieux d'une semaine, on l'efface de la bdd et on l'affiche pas

							if(  $end_date + 604800 < time()   )	
							{
								$deleteBet -> execute(array($donneesBets['idparis']));
								$deleteVote -> execute(array($donneesBets['idparis']));
								$deleteVote -> closeCursor();
								$deleteBet -> closeCursor();
							}
							else{
								?>
								<div class="col-md-6  col-xs-12">
									<i class="fa fa-users" aria-hidden="true"></i><?php echo $donneesgroup['nom_groupe']; ?></br>
									<div class="col-md-12  col-xs-12 bottom_bet shadow">
										
										<div class="row">

											<div class="col-md-12 col-xs-12 bg-primary center_text">

												<div class="row">
													<div class="col-md-4 col-xs-3 num textBet">
														<i class="fa fa-tag" aria-hidden="true"></i>
														n°<?php 
														echo $num;
														$num += 1;
														?>								    					
													</div>
												</div>

											</div>

											<div class="col-md-12 col-xs-12 bg-primary">
												<h5>
													<?php
													echo $donneesBets['nom_paris'];
													?>
												</h5>

											</div>
											<div class="col-md-12 col-xs-12 bg-primary">

												<?php
												$donneesParis = $getParis->fetch();
												$currentTime = time();
												$endTime = mktime( $donneesBets['end_date'][11].$donneesBets['end_date'][12], 
													$donneesBets['end_date'][14].$donneesBets['end_date'][15],
													$donneesBets['end_date'][17].$donneesBets['end_date'][18],
													$donneesBets['end_date'][5].$donneesBets['end_date'][6],
													$donneesBets['end_date'][8].$donneesBets['end_date'][9],
													$donneesBets['end_date'][0].$donneesBets['end_date'][1].$donneesBets['end_date'][2].$donneesBets['end_date'][3]);

												$remainingTime = $endTime - $currentTime;

												?>

												<form method="post" action="bet.php?id=<?php echo $donneesBets['idparis']?>">

													<div class="row">

														<?php 
														if($remainingTime>0){	
															?>	
															<button type"submit" class="btn btn-info btn-lg no_radius btn-block" >	  
																Bet now!
																<?php
															}else{
																?>
																<button type"submit" class="btn btn-warning btn-lg no_radius btn-block" >	  
																	See results
																	<?php
																}
																?>


															</button>
														</div>



													</form>


												</div>

												<!--**************************************************************** info bet **********************************************************************-->

												<div class="col-md-12 col-xs-12">
													<div class="row">
														<div class="col-md-6 col-xs-6">
															<?php
															$pseudo;
															$getNameUser->execute(array($donneesBets['idadmin_paris']));
															while($name = $getNameUser->fetch())
															{
																$pseudo = $name['pseudo'];
															}
															$getNameUser->closeCursor();
															?>
															<a href="profil_public.php?pseudo=<?php echo $pseudo?>">
																<i class="fa fa-user fa-fw center_text textBet"></i>
																<?php

																echo $pseudo;
																?>
															</a>
														</div>
														<div class="col-md-6 col-xs-6 text-primary center_text textBet">
															<i class="fa fa-clock-o fa-fw"></i>

															<?php

															$donneesParis = $getParis->fetch();
															$currentTime = time();
															$endTime = mktime( $donneesBets['end_date'][11].$donneesBets['end_date'][12], 
																$donneesBets['end_date'][14].$donneesBets['end_date'][15],
																$donneesBets['end_date'][17].$donneesBets['end_date'][18],
																$donneesBets['end_date'][5].$donneesBets['end_date'][6],
																$donneesBets['end_date'][8].$donneesBets['end_date'][9],
																$donneesBets['end_date'][0].$donneesBets['end_date'][1].$donneesBets['end_date'][2].$donneesBets['end_date'][3]);

															$remainingTime = $endTime - $currentTime;
															if($remainingTime>0){
										    							 			$rd = (int)($remainingTime / 86400); //jours
										    							 			$rh = (int)(($remainingTime - ($rd*86400))/3600); //heures
										    							 			if($rd>0){
										    							 				echo $rd.' days | '.$rh.' hours';
										    							 			}else{
										    							 				echo $rh.' hours';
										    							 			}
										    							 		}else{
										    							 			echo '<font color="orange">End date reached !</font>';
										    							 		}


										    							 		?>
										    							 	</div>
										    							 </div>
										    							</div>
										    						</div>
										    						<div class="col-md-12 col-xs-12 text-primary text_bet textBet">

										    							<i class="fa fa-quote-left" aria-hidden="true"></i>
										    							<?php

										    							echo $donneesBets['description']
										    							?>
										    							<i class="fa fa-quote-right" aria-hidden="true"></i>
										    						</div>

										    					</div>
										    				</div>	
										    				<?php	
										    			}}}
										    			$groupeBets->closeCursor();
										    			?>
										    		</div>


										    	</div>

										    </div>



										    <!--**************************************************************** Pager bets **********************************************************************-->


										    <nav>
										    	<ul class="pager">
										    		<?php 
										    		if ($numPage != 0)
										    		{
										    			?> 
										    			<li class="pager-prev"><a href="groupe.php?id=<?php echo $_GET['id']?>&page=<?php echo $numPage-1;?>"><strong>previous</strong></a></li>
										    			<?php
										    		}
										    		$nbrParis = 0;
										    		$groupeBets->execute(array($_GET['id']));
										    		while($donnees = $groupeBets->fetch())
										    		{
										    			$nbrParis += 1;
										    		}
										    		if ($nbrParis-4*$numPage > 4)
										    		{
										    			?> 

										    			<li class="pager-next"><a href="groupe.php?id=<?php echo $_GET['id']?>&page=<?php echo $numPage+1;?>"><strong>next</strong></a></li>

										    			<?php
										    		}
										    		?>					
										    	</ul>
										    </nav>



										</div>
									</div>
								</div>

								<!-- jQuery first, then Bootstrap JS. -->
								<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
								<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
							</div>
						</body>
						</html>
