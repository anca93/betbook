<?php

	//*** bet_adminAsnswer permet à l'ADMIN du pari de donner la bonne réponse APRES échéance ***//
	//*** affiche toutes les réponses en bleu ciel (btn-info) ***//


	session_start();
	//variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//


    if(isset($_GET['id']))
    {
    	$_GET['id'] = (int)htmlspecialchars($_GET['id']);

    	if($_GET['id'] <= 0)
    	{
    		header('Location: main.php'); 
    	}
    }
    
    //verifie l'acces au pari comme admin
	   	//**********************************************************************************//   
	   	
	   	$getParis->execute(array($_GET['id']));
	    $donneesParis = $getParis->fetch();

	    if($idMembre != $donneesParis['idadmin_paris'])
	   	{
	   		header('Location: main.php'); 
	   	}
	  	//**********************************************************************************//


    //calcule le nombre de paris de l'utilisateur connecté
  	//**********************************************************************************//
    $nbrBets->execute();
    $nbrParis = 0;
    while($donnees = $nbrBets->fetch())
    {
    	$nbrParis += 1;
    }
   	$nbrBets->closeCursor();  	
   	//**********************************************************************************//
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">

        <?php include("utils/leftmenu.php"); ?>

        <div class="col-md-9 top">
        	<div class="container">
	        	<div class="row">
					<?php
	   					$getParis->execute(array($_GET['id']));
						$donneesParis = $getParis->fetch();

						$getAnswer->execute(array($idMembre, $_GET['id']));
						$donneesAnswer = $getAnswer->fetch();
					?>
		       		<div class="col-md-12 col-xs-12 bg">
		       			<h2><?php echo $donneesParis['nom_paris'];?></h2>
		       			<h3><?php echo $donneesParis['description'];?></h3>
			        	<div class="col-lg-12 col-md-12">
			        		<div class="row">
			        			<div class="col-md-12  col-xs-12">
						    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
							    		<div class="row">
							    			<div class="col-md-12 col-xs-12 bg-primary center_text">
									    		<p >
									    			<i class="fa fa-newspaper-o fa-5x" aria-hidden="true"></i>
									    			<h1>Select the good answer for your bet</h1>
											    	</p>
									    	</div>

									    	<div class="btn-group-vertical btn-block no_radius">
									    		<?php 
								    			if($donneesParis['answer1'] != 'NONE')
								    			{ 
								    				?>
									    			<form action="postGoodAnswer.php" method="POST">
									    			<input type="hidden" value="1" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <button type"submit" class="btn btn-info btn-lg no_radius btn-block" >	  
												    	<div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer1']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 1)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												    </form>
												<?php
												}
									    		?>

									    		<?php 
								    			if($donneesParis['answer2'] != 'NONE')
								    			{ 
								    				?>
									    			<form action="postGoodAnswer.php" method="POST">
									    			<input type="hidden" value="2" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <button type"submit" class="btn btn-info btn-lg no_radius btn-block" >
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer2']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 2)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
													    	</div>
												    	</div>
												    </button>
												    </form>
												<?php
												}
									    		?>

									    		<?php 
								    			if($donneesParis['answer3'] != 'NONE')
								    			{ 
								    				?>
									    			<form action="postGoodAnswer.php" method="POST">
									    			<input type="hidden" value="3" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <button type"submit" class="btn btn-info btn-lg no_radius btn-block" >
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer3']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 3)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												    </form>
												<?php
												}
									    		?>
									    		<?php 
								    			if($donneesParis['answer4'] != 'NONE')
								    			{ 
								    				?>
									    			<form action="postGoodAnswer.php" method="POST">
									    			<input type="hidden" value="4" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <button type"submit" class="btn btn-info btn-lg no_radius btn-block" >
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer4']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 4)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												    </form>
												<?php
												}
									    		?>
									    		<?php 

								    			if($donneesParis['answer5'] != 'NONE')
								    			{ 
								    				?>
									    			<form action="postGoodAnswer.php" method="POST">
									    			<input type="hidden" value="5" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <button type"submit" class="btn btn-info btn-lg no_radius btn-block" >
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer5']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 5)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												    </form>
												<?php
												}
									    		?>

									    	</div>
									    </div>
								    </div>	
				        		</div>
				        	</div>
				        </div>

				        <div class="row">
			        		<div class="col-lg-5 col-md-12">
			        			<div class="container">
						    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
							    		<div class="row">
							    			<div class="col-md-12 col-xs-12 bg-primary center_text">
									    			<p >
									    			<i class="fa fa-info-circle fa-5x" aria-hidden="true"></i>
											    	</p>
									    	</div>
									    	<div class="col-md-12 col-xs-12 text-primary">
									    		<li>
													Posted by :
													<a href="main.php">
										    			<?php 
										    				$getNameUser->execute(array($donneesParis['idadmin_paris']));
										    				$name = $getNameUser->fetch();
										    				echo $name['pseudo'];
										    			?>

										    		</a>
										    	</li>
									    	</div>

									    	<?php 
									    				$getGroupFromBet->execute(array($_GET['id']));
									    				$name = $getGroupFromBet->fetch();
									    	?>

									    	<div class="col-md-12 col-xs-12 text-primary">
												<li>
													<a href="groupe.php?id=<?php echo $name['idgroupe']?>">
										    			<i class="fa fa-users fa-fw "></i>
										    			<?php 
										    				echo $name['nom_groupe'];
										    			?>
										    		</a>
										    	</li>
									    	</div>
									    	<div class="col-md-12 col-xs-12 text-primary">
									    		<li
									    			<a>
											    		<i class="fa fa-calendar fa-fw"></i>
									    				<?php 
									    					$date = date_parse($donneesParis['creation_date']);
								    						echo $date['month'].'/'.$date['day'].'/'.$date['year']
									    				?>
								    				</a>	
								    			</li>
									    	</div>

											<div class="col-md-12 col-xs-12 text-primary">
												<li>
									    			<i class="fa fa-building fa-fw "></i>
									    			<?php echo $donneesParis['ville'];?>
									    		</li>
									    	</div>

									    	<div class="col-md-12 col-xs-12 text-primary ">
									    		<li>
									    			<i class="fa fa-industry fa-fw"></i>
									    			<?php 
									    			if($donneesParis['entreprise'] != '')
									    			{
									    				echo $donneesParis['entreprise'];
									    			}
									    			else
									    			{
									    				echo 'none';
									    			}
									    			?>
									    		</li>
									    	</div>
									    </div>
							    	</div>	
							    </div>
						    </div>
							<div class="col-lg-7 col-md-12">
							
							
							<div class=" container">
				        		<div class="row">
				        			<div class="col-md-12  col-xs-12">
							    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
								    		<div class="row">
								    			<div class="col-md-12 col-xs-12 bg-primary center_text">
										    		<p >
										    			<i class="fa fa-users fa-5x" aria-hidden="true"></i>
												    	</p>
										    	</div>
										    	<div>
										    		<div class="container text-primary">
											    		<?php
											    			$getUsers->execute(array($_GET['id']));
											    			while( $users = $getUsers->fetch())
											    			{
											    				$getNameUser->execute(array($users['idmembre']));
											    				$pseudo = $getNameUser->fetch()
											    				?>
											    				<li>
											    					<a href="profil_public.php?pseudo=<?php echo $pseudo['pseudo']?>"><?php echo $pseudo['pseudo']?></a>
											    						bet on "
											    						<?php 
											    							$getAnswer->execute(array($users['idmembre'], $_GET['id']));
											    							$idAnswer = $getAnswer->fetch();
											    							$getParis->execute(array($_GET['id']));
											    							$donneesParis = $getParis->fetch();
											    							$answer = 'answer';
											    							$answer = 'answer'.$idAnswer['idreponse_vote'];
											    							echo $donneesParis[$answer];
											    						?>"
											    				</li>
											    				<?php
											    				$getNameUser->closeCursor();
											    			}	
											    			$getUsers->closeCursor();
											    		?>
											    	</div>
										    	</div>
										    </div>
										</div>
								    </div>	
				        		</div>
				        	</div>
			        	</div>
			        </div>
	        	</div>
	        </div>
        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</body>
</html>
