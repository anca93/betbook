<?php
    
    if(session_start()==0 || $_SESSION['idmembre'] ==0)
  {
    header('Location: inscription.php'); 
    exit();
  }

    //variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idmembre =  $_SESSION['idmembre'];
    
    //**********************************************************************************//

    //include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    if(!isset($_GET['ranking'])){
      header('Location: ranking.php?ranking=1');
    }


	
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>BetBook|The social betting</title> 
      <link rel="icon" href="utils/image/logo.ico" />

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

    
    <title>BetBook|The social betting</title>  


</head>
<body>


<?php include("utils/navbar.php");?>

<div class="container">
    <div class="row">
      
    <?php include("utils/leftmenu.php");?>

    <div class="col-md-9 col-md-9 top container">
   		<div class="col-md-12 col-xs-12 bg">
   			<div class="row">

    			<h1 class="titre">Ranking</h1>


 
        		
        		<div class="container">
	        		<div class="row">
                
                <div class="col-md-12 col-xs-12">
                  <div class="col-md-3 col-xs-3 subpolice">
                    
                    <a type="button" class="btn btn-primary" href="ranking.php?ranking=1">World</a>
                  </div>

                  <div class="col-md-3 col-xs-3 subpolice_group dropdown">



                  <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Groups
                    <span class="caret"></span></button>

                    <ul class="dropdown-menu">

                      <?php 
                      $recupgroup->execute(array('idmembre' => $_SESSION['idmembre'])); 
                      while ($donnees = $recupgroup->fetch()){
                        ?>

                      <li><a href="ranking.php?ranking=2&idgroup=<?php echo $donnees['idgroupe']?>"><?php echo $donnees['nom_groupe']; echo " "; ?></a></li>
                      
                      <?php
                      }
                      ?>

                    </ul>
                  </div>


                  </br>

                </div>
                <div class="col-md-3 col-xs-3 subpolice">
                  <a type="button" class="btn btn-primary" href="ranking.php?ranking=3">Company</a>
                </br>
                </div>
                <div class="col-md-3 col-xs-3 subpolice">
                  <a type="button" class="btn btn-primary" href="ranking.php?ranking=1">City</a>
                </div>
              </div>         

                </div>
                <div class="col-md-4 col-xs-4 subpolice">
                <strong> <?php   
                  if(isset($_GET['ranking'])){
                
                  if($_GET['ranking']==1){
                $recupclass->execute();
                    $recupid->execute();
                    

                    $donnees = $recupclass->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    


                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupid->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }
                    }
                  }
                  if($_GET['ranking']==2){
                if (isset($_GET['idgroup'])){

                      $recupclassgroup->execute(array('idgroupe' => $_GET['idgroup']));
                      $recupidgroup->execute(array('idgroupe' => $_GET['idgroup']));

                      $donnees = $recupclassgroup->fetch();
                      $nb=$donnees['class'];
                      $nb=intval($nb);

                      for ($i = 1; $i <= $nb; $i++) {
                        $donnees2 = $recupidgroup->fetch();
                        if($donnees2['idmembre'] == $_SESSION['idmembre']){
                          ?><b><font size="4"><?php echo $i; ?></font></b><?php
                          echo "<br>";
                        }

                      }
                    }
                  }
                  if($_GET['ranking']==3){
                $recupclassentreprise->execute(array('idmembre' => $_SESSION['idmembre']));
                    $recupidentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    $donnees = $recupclassentreprise->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupidentreprise->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }
                    }
                  }
                  if($_GET['ranking']==4){
                $recupclassville->execute(array('idmembre' => $_SESSION['idmembre']));
                    $recupidville->execute(array('idmembre' => $_SESSION['idmembre']));

                    $donnees = $recupclassville->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupidville->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }
                    }
                  }
                }
                    ?></strong>
                </div>

                <div class="col-md-4 col-xs-4 subpolice">
                <strong> <?php   
                if(isset($_GET['ranking'])){
                
                $recupname->execute(array('idmembre'=>$_SESSION['idmembre']));
                    
                    $name=$recupname->fetch();
                    
                    echo $name['pseudo'];
                  
                }
                    ?></strong>
                </div>
                <div class="col-md-4 col-xs-4 subpolice">
                <strong> <?php   
                  if(isset($_GET['ranking'])){
                
                  if($_GET['ranking']==1){
                $recupscoreuser->execute(array('idmembre'=>$_SESSION['idmembre']));
                  $scoreuser=$recupscoreuser->fetch();
                  echo $scoreuser['score'];
                  }
                  if($_GET['ranking']==2){
                    if(isset($_GET['idgroup'])){
                $recupscoregroupeuser->execute(array('idmembre'=>$_SESSION['idmembre'],'idgroupe'=>$_GET['idgroup']));
                  $scoregroup=$recupscoregroupeuser->fetch();
                  echo $scoregroup['score'];
                }
                  }
                  if($_GET['ranking']==3){
                $recupscoreentreprise->execute(array('idmembre'=>$_SESSION['idmembre']));
                  $scoreentreprise=$recupscoreentreprise->fetch();
                  echo $scoreentreprise['score'];
                  }
                  if($_GET['ranking']==4){
                $recupscoreville->execute(array('idmembre'=>$_SESSION['idmembre']));
                  $scoreville=$recupscoreville->fetch();
                  echo $scoreville['score'];
                  }
                }
                    ?></strong>
                </div>
              <?php 
 
                    $checkentreprise->execute(array('idmembre' => $_SESSION['idmembre']));
                    $check1=$checkentreprise->fetch();
                    $checkcity->execute(array('idmembre' => $_SESSION['idmembre']));
                    $check2=$checkcity->fetch();

                    if($check1['entreprise']=='' && $_GET['ranking']==3){
                    
                     ?><h3_noline> You didn't mention your firm in your profile !</h3_noline></br>
                      <a href="settings2.php" class="subpolice_error">Modify your profile</a><?php

                    }else if($check2['ville']=='' && $_GET['ranking']==4){
                      ?><h3_noline> You didn't mention your city in your profile !</h3_noline></br>
                      <a href="settings2.php" class="subpolice_error">Modify your profile</a><?php
                    }else{?>
              <div class="col-md-12 col-xs-12 backstrip">
                <div class="col-md-4 col-xs-4  subpolice">
                  Rank

                </div>

                <div class="col-md-4 col-xs-4 subpolice">
                  Pseudo
                </div>
                <div class="col-md-4 col-xs-4 subpolice">
                  Points
                </div>
              </div>

              <div class="col-md-12 col-xs-12 backrank">
                <div class="col-md-4 col-xs-4 subrank">
                  <?php 
                  if (isset($_GET['ranking']) AND $_GET['ranking']==1){
                    $recupclass->execute();
                    $recupid->execute();
                    

                    $donnees = $recupclass->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    


                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupid->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }else{
                        echo $i;
                        echo "<br>";
                      }
                      
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==2){

                    if (isset($_GET['idgroup'])){

                      $recupclassgroup->execute(array('idgroupe' => $_GET['idgroup']));
                      $recupidgroup->execute(array('idgroupe' => $_GET['idgroup']));

                      $donnees = $recupclassgroup->fetch();
                      $nb=$donnees['class'];
                      $nb=intval($nb);

                      for ($i = 1; $i <= $nb; $i++) {
                        $donnees2 = $recupidgroup->fetch();
                        if($donnees2['idmembre'] == $_SESSION['idmembre']){
                          ?><b><font size="4"><?php echo $i; ?></font></b><?php
                          echo "<br>";
                        }else{
                          echo $i;
                          echo "<br>";
                        }

                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==3){
                    


                    $recupclassentreprise->execute(array('idmembre' => $_SESSION['idmembre']));
                    $recupidentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    $donnees = $recupclassentreprise->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupidentreprise->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }else{
                        echo $i;
                        echo "<br>";
                      }
                      
                    }
                  }
                

                  if (isset($_GET['ranking']) AND $_GET['ranking']==4){
                    $recupclassville->execute(array('idmembre' => $_SESSION['idmembre']));
                    $recupidville->execute(array('idmembre' => $_SESSION['idmembre']));

                    $donnees = $recupclassville->fetch();
                    $nb=$donnees['class'];
                    $nb=intval($nb);

                    for ($i = 1; $i <= $nb; $i++) {
                      $donnees2 = $recupidville->fetch();
                      if($donnees2['idmembre'] == $_SESSION['idmembre']){
                        ?><b><font size="4"><?php echo $i; ?></font></b><?php
                        echo "<br>";
                      }else{
                        echo $i;
                        echo "<br>";
                      }
                      
                    }
                  }
                  ?>
                </div>

                <div class="col-md-4 col-xs-4 subrank">
                  <?php 
                  if (isset($_GET['ranking']) AND $_GET['ranking']==1){
                    $recuppseudo->execute();

                    while ($donnees = $recuppseudo->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['pseudo']; 
                        echo "<br>";
                      }

                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==2){

                    if (isset($_GET['idgroup'])){
                      $recuppseudogroup->execute(array('idgroupe' => $_GET['idgroup']));

                      while ($donnees = $recuppseudogroup->fetch()){
                        if ($donnees['idmembre']==$_SESSION['idmembre']) {
                          ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                          echo "<br>";
                        }else{
                          echo $donnees['pseudo']; 
                          echo "<br>";
                        }

                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==3){
                    $recuppseudoentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recuppseudoentreprise->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['pseudo']; 
                        echo "<br>";
                      }

                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==4){
                    $recuppseudoville->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recuppseudoville->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['pseudo']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['pseudo']; 
                        echo "<br>";
                      }

                    }
                  }
                  ?>
                </div>

                <div class="col-md-4 col-xs-4 subrank">
                  <?php
                  if (isset($_GET['ranking']) AND $_GET['ranking']==1){
                    $recupscore->execute();

                    while ($donnees = $recupscore->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['score']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['score']; 
                        echo "<br>";
                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==2){
                    if (isset($_GET['idgroup'])){
                      $recupscoregroup->execute(array('idgroupe' => $_GET['idgroup']));

                      while ($donnees = $recupscoregroup->fetch()){
                        if ($donnees['idmembre']==$_SESSION['idmembre']) {
                          ?><b><font size="4"><?php echo $donnees['score_groupe']; ?></font></b><?php  
                          echo "<br>";
                        }else{
                          echo $donnees['score_groupe']; 
                          echo "<br>";
                        }
                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==3){
                    $recupscoreentreprise->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recupscoreentreprise->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['score']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['score']; 
                        echo "<br>";
                      }
                    }
                  }

                  if (isset($_GET['ranking']) AND $_GET['ranking']==4){
                    $recupscoreville->execute(array('idmembre' => $_SESSION['idmembre']));

                    while ($donnees = $recupscoreville->fetch()){
                      if ($donnees['idmembre']==$_SESSION['idmembre']) {
                        ?><b><font size="4"><?php echo $donnees['score']; ?></font></b><?php  
                        echo "<br>";
                      }else{
                        echo $donnees['score']; 
                        echo "<br>";
                      }
                    }
                  }
                  ?>
                </div>

              </div>
              <?php }?>

        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</div>
</body>
</html>
