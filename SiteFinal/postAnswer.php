<?php

	session_start();
	//variable à initialiser avec les variable de session
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//
	
	//connection à la BDD
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //analyse de answer
	if(isset($_POST['answer']) AND isset($_POST['idParis']))
	{
		
		$_POST['answer'] = (int)htmlspecialchars($_POST['answer']);
		$_POST['idParis'] = (int)htmlspecialchars($_POST['idParis']);

		if($_POST['answer'] > 5 or $_POST['answer'] < 0)
		{
			header('Location: main.php'); 
		}

	   	//verifie l'acces au pari
	   	//**********************************************************************************//   
	   	$present = 0;
	   	
	   	$getParis->execute(array($_POST['idParis']));
	    $donneesParis = $getParis->fetch();

	    $header->execute();

	    while($donneesHeader = $header->fetch())
		{
	        if($donneesHeader['idgroupe'] == $donneesParis['idgroupe'])
	    	{
	    		$present = 1;
		    }
	    }
	   		
	    $getParis->closeCursor();   	
	    $header->closeCursor();

	   	if($present != 1)
	   	{
	   		header('Location: main.php'); 
	   	}
	  	//**********************************************************************************//

		$addAnswer->execute(array($_POST['idParis'],$idMembre,$_POST['answer']));

		header('Location: bet.php?id='.$_POST['idParis']); 
	}
	else
	{
		header('Location: main.php'); 
	} 
?>