<?php

    session_start();
    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idMembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et la bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    if ( ! isset($_GET['idgroup'])) {
        header('default_page.php');
        echo "<div class=\"error\">";
        echo 'Bad URL : No group specified';
        echo "</div>";
        echo '</br>';
        
    }

    $idGroup = $_GET['idgroup']; 

    $req = $bdd->prepare('INSERT INTO Jonction(idmembre, idgroupe, score_groupe) VALUES(:idmembre, :idgroupe, :score_groupe)');

    $req->execute(array('idmembre' => $idMembre , 'idgroupe' => $idGroup, 'score_groupe' => 0));

    header('Location: groupe.php?id='.$idGroup);

    exit();

?>