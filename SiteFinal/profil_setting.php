<?php




if(session_start()==0 || $_SESSION['idmembre'] ==0)
  {
    header('Location: inscription.php'); 
    exit();
  }
ini_set('display_errors', 1);
//include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //variable à initialiser avec les variable de scession
$idMembre = $_SESSION['idmembre'];

?>  

  <!-- CSS perso -->
  <link rel="stylesheet" href="perso.css">

<body>

          <div class="col-md-12 col-xs-12 bg3">
            <div class="row">

              <h1 class="titre">Welcome home, <?php echo $_SESSION['pseudo'];?> !</h1>
            </br>

            <div class="row ">
              <div class="col-md-6 col-xs-12">

<!--*********************************************************Picture**********************************************************************-->

                
                  <?php
                  if (isset($_GET['image_change']) AND ($_GET['image_change']==1 )){
                   $image = $_SESSION['image'];
                 }
                 $image = $_SESSION['image']; 
                 ?>

                  </br>
                  </br>
                  </br>
                 <img src="http://localhost/projet_web/profil/image/<?php echo $image?>" 
                 width="210" height="200" alt="" title="" style="float:left;"></img>
                 <?php
                 if( !empty($message) ) 
                 {
                  echo '<p>',"\n";
                  echo "\t\t<strong>", htmlspecialchars($message) ,"</strong>\n";
                  echo "\t</p>\n\n";
                }
                ?>

                <!-- Debut du formulaire -->
                <script type="text/javascript">
    /* Voici la fonction javascript qui change la propriété "display"
    pour afficher ou non le div selon que ce soit "none" ou "block". */

                  function AfficherMasquer(arg)
                  {
                    divInfo = document.getElementById(arg);

                    if (divInfo.style.display == 'none')
                      divInfo.style.display = 'block';
                    else
                      divInfo.style.display = 'none';

                  }
                </script>

                  <ul><a> <button class="buttons"  onClick="AfficherMasquer('divacacher')" ><span class="add"></span></button> </a></ul>

                  <div id="divacacher" style="display:none;">
                   <form enctype="multipart/form-data" action="upload.php" method="post">
                    <fieldset>

                      <legend>Choose a picture !</legend>
                      <p>
                        <label for="fichier_a_uploader" title="Recherchez le fichier à uploader !">Send this picture :</label>
                        <input type="hidden" name="MAX_FILE_SIZE"  value="<?php echo MAX_SIZE; ?>" />
                        <input name="fichier" type="file" id="fichier_a_uploader" />
                       <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                      </p>
                    </fieldset>
                  </form>
                </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                      </br>
                      <p><span class="title">Personal Informations:</span><p>
                      </br>
<!--***********************************************************Pseudo**********************************************************************-->


                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-user fa-fw bg2 ellipsis"></i>
                          <a><button  class="buttons droite"  onClick="AfficherMasquer('divpseudo')"><span class="add"></span></button> </a>
                          
                          <?php
                          echo '<span class="champs">'.$_SESSION['pseudo'].'</span>';
                          ?>
                        </a>
                      </ul>
                      <?php
                      if (isset($_GET['pseudo_utilise']) AND ($_GET['pseudo_utilise']==0 )){
    
                       echo '<p><span class=invalide>This login is already taken..<span></p>';   
                      }

                      if (isset($_GET['new_pseudo']) AND ($_GET['new_pseudo']==0 )){
    
                       echo '<p><span class=invalide>Beetween 4 and 15 letter or number<span></p>';   
                      }

                      ?>
                      <div id="divpseudo" style="display:none;">
                         <form action="update.php" method="post">
                          <input type="text" name="new_pseudo" placeholder="New pseudo" />
                          <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                        </form>

                      </div>
<!--***********************************************************Mail**********************************************************************-->

                     <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-envelope-o fa-fw bg2 ellipsis"></i>
                          <a><button  class="buttons droite"  onClick="AfficherMasquer('divmail')"><span class="add"></span></button> </a>
                          
                          <?php
                          echo '<span class="champs">'.$_SESSION['mail'].'</span>';
                          ?>
                        </a>
                      </ul>
                      <?php
                      if (isset($_GET['email_utilise']) AND ($_GET['email_utilise']==0 )){
    
                       echo '<p><span class=invalide>This email is already taken..<span></p>';   
                      }

                      if (isset($_GET['new_mail']) AND ($_GET['new_mail']==0 )){
    
                       echo '<p><span class=invalide>Email incorrect<span></p>';   
                      }

                      ?>

                       <div id="divmail" style="display:none;">
                         <form action="update.php" method="post">
                          <input type="text" name="new_mail" placeholder="Email" />
                          <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                        </form>

                      </div>
<!--***********************************************************Password****************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-key fa-fw bg2 ellipsis"></i>
                          <a><button  class="buttons droite"  onClick="AfficherMasquer('divpassword')"><span class="add"></span></button> </a>
                          
                          <?php
                          echo '<span class="champs">Password</span>';
                          ?>
                        </a>
                      </ul>
                      <?php

                      if (isset($_GET['new_password']) AND ($_GET['new_password']==0 )){
    
                       echo '<p><span class=invalide>Beetween 4 and 15 letter or number<span></p>';   
                      }

                      if (isset($_GET['same_password']) AND ($_GET['same_password']==0 )){
    
                       echo '<p><span class=invalide>Password incorrect<span></p>';   
                      }

                      if (isset($_GET['password_valid']) AND ($_GET['password_valid']==0 )){
    
                       echo '<p><span class=invalide>Password changed<span></p>';   
                      }



                      ?>

                       <div id="divpassword" style="display:none;">
                         <form action="update.php" method="post">
                          <input type="password" name="old_password" placeholder="Old Password"  />
                          <input type="password" name="new_password" placeholder="New Password"  />

                          <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                        </form>

                      </div>
<!--*********************************************************Birthday**********************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-birthday-cake fa-fw bg2 ellipsis"></i>
                          <a><button  class="buttons droite"  onClick="AfficherMasquer('divbirthday')"><span class="add"></span></button> </a>
                          
                          <?php
                          echo '<span class="champs">'.$_SESSION['birthday'].'</span>';
                          ?>
                        </a>
                      </ul>

                      <?php

                      if (isset($_GET['new_birthday']) AND ($_GET['new_birthday']==0 )){
    
                       echo '<p><span class=invalide>yyyy-mm-dd<span></p>';   
                      }

                      ?>
                       <div id="divbirthday" style="display:none;">
                         <form action="update.php" method="post">
                          <input type="text" name="new_birthday" placeholder="aaaa-mm-jj" />
                          <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                        </form>

                      </div>
<!--*********************************************************City**********************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-building fa-fw bg2 ellipsis"></i>
                          <a><button  class="buttons droite"  onClick="AfficherMasquer('divcity')"><span class="add"></span></button> </a>
                          
                          <?php
                          echo '<span class="champs">'.$_SESSION['ville'].'</span>';
                          ?>
                        </a>
                      </ul>
                      <?php

                      if (isset($_GET['new_city']) AND ($_GET['new_city']==0 )){
    
                       echo '<p><span class=invalide>This city does not exist !<span></p>';   
                      }

                      ?>

                       <div id="divcity" style="display:none;">
                         <form action="update.php" method="post">
                          <input type="text" name="new_city" placeholder="City" />
                          <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                        </form>

                      </div>
<!--***********************************************************University******************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-university fa-fw bg2 ellipsis"></i>
                          <a><button  class="buttons droite"  onClick="AfficherMasquer('divcompany')"><span class="add"></span></button> </a>
                          
                          <?php
                          echo '<span class="champs">'.$_SESSION['entreprise'].'</span>';
                          ?>
                        </a>
                      </ul>

                      <?php

                      if (isset($_GET['new_university']) AND ($_GET['new_university']==0 )){
    
                       echo '<p><span class=invalide>This school is very bad !<span></p>';   
                      }

                      ?>

                       <div id="divcompany" style="display:none;">
                         <form action="update.php" method="post">
                          <input type="text" name="new_university" placeholder="University/Company" />
                          <a><button  type="submit"  class="buttons " name="submit"><span class="tick"></span>  </button> </a>
                        </form>

                      </div>
<!--***********************************************************Score**********************************************************************-->

                      <ul class="nav-item active">
                        <a class="nav-link"><i class="fa fa-trophy fa-fw bg2 ellipsis"></i>
                          
                          <?php
                          echo '<span class="champs">'.$_SESSION['score'].' Points</span>';
                          ?>
                        </a>
                      </ul>                    
                  </div>

</div>



</div>
</div>
</div>


</body>

