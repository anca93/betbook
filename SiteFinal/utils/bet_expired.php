			        	<div class="col-lg-12 col-md-12">
			        		<div class="row">
			        			<div class="col-md-12  col-xs-12">
						    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
							    		<div class="row">
							    			<div class="col-md-12 col-xs-12 bg-primary center_text">
									    		<p >
									    			<?php 
									    				if ($donneesParis['good_answer'] > 0)
									    				{ 
									    			?>
									    				<h4>And the right answer was ... </h4>
									    			<?php
									    				}
									    				else
									    				{
									    			?>
									    				<h4>Bet expired. The administrator has not chosen the right answer yet.</h4>
									    			<?php
									    				}
									    			?>
											    	</p>
									    	</div>

									    	<div class="btn-group-vertical btn-block no_radius">
									    		<?php 
								    			if($donneesParis['answer1'] != 'NONE')
								    			{ 
								    				?>
									    			
									    			<input type="hidden" value="1" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">

												    <?php
												    	if($donneesParis['good_answer'] == 1)
												    	{
												    ?>
												    	<button class="btn btn-success btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    	else
												    	{
												    ?>
												    	<button class="btn btn-danger btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    ?>

												    	  <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer1']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 1)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												 
												<?php
												}
									    		?>

									    		<?php 
								    			if($donneesParis['answer2'] != 'NONE')
								    			{ 
								    				?>
									    		
									    			<input type="hidden" value="2" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <?php
												    	if($donneesParis['good_answer'] == 2)
												    	{
												    ?>
												    	<button class="btn btn-success btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    	else
												    	{
												    ?>
												    	<button class="btn btn-danger btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    ?>
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer2']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 2)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
													    	</div>
												    	</div>
												    </button>
												
												<?php
												}
									    		?>

									    		<?php 
								    			if($donneesParis['answer3'] != 'NONE')
								    			{ 
								    				?>
									    	
									    			<input type="hidden" value="3" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <?php
												    	if($donneesParis['good_answer'] == 3)
												    	{
												    ?>
												    	<button class="btn btn-success btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    	else
												    	{
												    ?>
												    	<button class="btn btn-danger btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    ?>
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer3']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 3)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												
												<?php
												}
									    		?>
									    		<?php 
								    			if($donneesParis['answer4'] != 'NONE')
								    			{ 
								    				?>
									    			
									    			<input type="hidden" value="4" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <?php
												    	if($donneesParis['good_answer'] == 4)
												    	{
												    ?>
												    	<button class="btn btn-success btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    	else
												    	{
												    ?>
												    	<button class="btn btn-danger btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    ?>
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer4']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 4)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												  
												<?php
												}
									    		?>
									    		<?php 

								    			if($donneesParis['answer5'] != 'NONE')
								    			{ 
								    				?>
									    			
									    			<input type="hidden" value="5" name="answer">
									    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
												    <?php
												    	if($donneesParis['good_answer'] == 5)
												    	{
												    ?>
												    	<button class="btn btn-success btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    	else
												    	{
												    ?>
												    	<button class="btn btn-danger btn-lg no_radius btn-block" disabled>
												    <?php
												    	}
												    ?>
												    	 <div class="raw">
													    	<div class="col-md-10 col-xs-10 left-text">
													    		<?php echo $donneesParis['answer5']?>
													    	</div>
													    	<div class="col-md-2 col-xs-2">
													    	<?php
													    		if( $donneesAnswer['idreponse_vote'] == 5)
													    		{
													    	?>
													    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
													    	<?php
													    		}
													    		else
													    		{
													    	?>
													    		<i class="fa fa-square-o" aria-hidden="true"></i>	
													    	<?php
													    		}
													    	?>
												    	</div>
												    </button>
												
												<?php
												}
									    		?>

									    	</div>
									    </div>
								    </div>	
				        		</div>
				        	</div>
				        </div>