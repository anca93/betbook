<?php

	include 'sqlrequest.php';

    $idMembre =  $_SESSION['idmembre'];

    $image = $_SESSION['image'];

                 
?><div class="col-md-3 top container" >

        	<div class="col-md-12 col-xs-12 bg ">


				<?php

	            $count = 0;
			    $header->execute();
			    while($donneesHeader = $header->fetch())
			    {
         			$count += 1;
         		}
         		$header->closeCursor();
         		
	             ?>
	        
				<div class="col-md-12 col-xs-12 panel-group" style="padding: 20px; text-align: center;">		
					
					<a href='settings2.php'>
						<img src="utils/image/<?php echo $image?>" class = "img-circle profile" </img>	
					</a>

					<div class="col-md-12 col-xs-12 panel-group" style="padding-top: 10px;">		
							Welcome,  
							<?php 
							$getNameUser->execute(array($idMembre));
		    				$name = $getNameUser->fetch();
		    				echo $name['pseudo'];
							$getNameUser->closeCursor();
							?>
							!
					</div>		
				</div>
					<div class="col-md-12 col-xs-12 panel-group">
				    <div class="panel panel-default collapseTitle">
						<div class="panel-heading" style="text-align: center;">
						    <a data-toggle="collapse" href="#collapse" >
						    Menu
							<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
 							</a>
						</div>
					</div>

				    <div id="collapse" class="panel-collapse collapse" style="text-align: left;">
				        <div class="col-md-12 col-xs-12 panel-group">
				    <div class="panel panel-default collapseTitle ellipsis">
						<div class="panel-heading">
						    <a data-toggle="collapse" href="#collapse1">
						    <i class="fa fa-users fa-fw center_text ellipsis"></i> My groups</a>
						</div>
					</div>

				    <div id="collapse1" class="panel-collapse collapse">
				        <ul class="list-group">
			        	<?php
			        	if($count != 0)
			        	{
						    $header->execute();
						    while($donneesHeader = $header->fetch())
						    {
							?>		    	
	                        <li class="nav-item active">
	                            <a class="nav-link text-primary ellipsis" href="groupe.php?id=<?php echo $donneesHeader['idgroupe']?>">#<?php echo $donneesHeader['nom_groupe']?><span class="sr-only">(current)</span></a>
	                        </li>
	                         <?php
	                 		}
	                 	}
	                 	else
	                 	{
	                 		?>
	                    	<li class="nav-item active collapseItem">
	                            <a class="nav-link  ellipsis titleGroups " href="#">                               		<i class="fa fa-frown-o" aria-hidden="true"></i> No group yet</br><a href="new_group_form.php">Create your group !</a>
	                            </a>
	                        </li>
	                    	<?php
	                    }
	                    ?>						         
				        </ul>
				    </div>
				</div>
				<div class="col-md-12 col-xs-12 panel-group">
				    <div class="panel panel-default collapseTitle">
				        <div class="panel-heading ellipsis">
				            <a data-toggle="collapse" href="#collapse2">
				            <i class="fa fa-question-circle fa-fw center_text ellipsis"></i> My Bets</a>
				        </div>
				        <div id="collapse2" class="panel-collapse collapse">
					        <ul class="list-group">
					        	<?php
					        	if($count != 0)
					        	{
								    $header->execute();
								    while($donneesHeader = $header->fetch())
								    {
								    	$count = 0;
								    	$bets->execute(array($idMembre, $donneesHeader['idgroupe']));
									    while($donneesBets = $bets->fetch())
									    {
									    	$count += 1;
									    }
									    $bets->closeCursor();
									    if($count != 0)
									    {
									    ?>
									    	<li class="dropdownTitle ellipsis"><?php echo '<strong>'.$donneesHeader['nom_groupe'].'</strong>'?></li>
									    <?php
										}
									    $bets->execute(array($idMembre, $donneesHeader['idgroupe']));
									    while($donneesBets = $bets->fetch())
									    {
									    	?>
									    			<a class="ellipsis" href="bet.php?id=<?php echo $donneesBets['idparis']?>">
										    		<i> <?php echo $donneesBets['nom_paris']?> </i>

										    	</a></br>
									    	<?php
									    }
									    $bets->closeCursor();
								    }
								   	$header->closeCursor();
		                     	}
		                     	else
		                     	{
		                     	?>
		                        	<li class="nav-item active collapseItem">
		                                <a class="nav-link  ellipsis titleGroups" href="#">                               		
		                                <i class="fa fa-frown-o" aria-hidden="true"></i> No bet yet
		                                </a>
		                            </li>
		                        <?php
		                        }
		                        ?>						         
					        </ul>
					    </div>
					</div>
				</div>

			
				<div class="col-md-12 col-xs-12 panel-group">
				    <div class="panel panel-default collapseTitle">
				        <div class="panel-heading">
				            <a data-toggle="collapse" href="#collapse3">
				            <i class="fa fa-lightbulb-o fa-fw center_text ellipsis"></i> Notifications</a>
				        </div>
				        <div id="collapse3" class="panel-collapse collapse">
					        <ul class="list-group">
					        	<?php
					      		$recupinvite->execute(array('idmembre' => $_SESSION['idmembre']));
					      		$recupnbinvite->execute(array('idmembre' => $_SESSION['idmembre']));
					      		$donneesnbinvite = $recupnbinvite->fetch();

					        	if($donneesnbinvite['count']!=0){
								    while($donneesinvite = $recupinvite->fetch())
								    {
								    ?>
								    	<li class="dropdownTitle"><?php echo $donneesinvite['nom_groupe']?></li>
								       			<a href="" class="fa fa-thumbs-o-up">
										    		<i> Accept </i>

										    	</a>
										    	<a href="" class="fa fa-thumbs-o-down">
										    		<i> Decline </i>

										    	</a></br>
									    	<?php
									    }
								    
								   	$recupinvite->closeCursor();
		                     	}
		                     	else
		                     	{
		                     	?>
		                        	<li class="nav-item active collapseItem">
		                                <a class="nav-link  ellipsis titleGroups" href="#">                               		
		                                <i><i class="fa fa-frown-o" aria-hidden="true"></i> No invitations </i></a>
		                            </li>
		                        <?php
		                        }
		                        ?>						         
					        </ul>
					    </div>
					</div>
				</div>

				<div class="col-md-12 col-xs-12 ">
                    <a class="nav-link ellipsis" href="ranking.php">                               									
					<i class="fa fa-trophy" aria-hidden="true"></i> Ranking
	                </a>
                </div>

				<div class="col-md-12 col-xs-12 ">
                    <a class="nav-link ellipsis" href="new_group_form.php">                               									
                    <i class="fa fa-plus fa-fw center_text ellipsis"></i> Create a group
                    </a>
                </div>

				<div class="col-md-12 col-xs-12 ">
                    <a class="nav-link ellipsis" href="settings2.php">                               									
                    <i class="fa fa-wrench fa-fw center_text ellipsis"></i> Edit profile
                    </a>
                </div>

                <div class="col-md-12 col-xs-12 ">
                    <a class="nav-link  ellipsis " href="deconnexion.php">                               									
                    <i class="fa fa-ban fa-fw center_text ellipsis"></i> Logout
                    </a>
                </div>
				    </div>
				</div>

				
				
			</div>
	    </div>