<?php



    class Pari
    {
        private $idparis;
        private $nom;
        private $idmembre;
        private $creation_date;
        private $end_date;
        private $ville;
        private $entreprise;
        private $description;
        private $answer1,$answer2,$answer3,$answer4,$answer5;
        private $idGroup;
        

        

        public function getAnswers()
        {   
            $answers[] = $this->answer1;
            $answers[] = $this->answer2;
            $answers[] = $this->answer3;
            $answers[] = $this->answer4;
            $answers[] = $this->answer5;
            return $answers;
        } 


        public function getIdParis()
        {
            return $this->idparis;
        }

        public function getIdMembre()
        {
            return $this->idmembre;
        }

        public function getName()
        {
            return $this->nom;
        }

        public function getDescription() 
        {
            return $this->description;
        }

        public function getCreationDate()
        {
            return $this->creation_date;
        }

        public function getEndDate()
        {
            return $this->end_date;
        }

        public function getCity()
        {
            return $this->ville;
        }

        public function getCompany()
        {
            return $this->entreprise;
        }



/****** CONSTRUCTEUR *****/

        public function __construct(
            $newDescription,
            $newName,
            $newAnswer1,
            $newAnswer2,
            $newAnswer3,
            $newAnswer4,
            $newAnswer5,
            $newCity,
            $newCompany,
            $newEndDate,
            $newIdMembre,
            $newIdGroup
            )
        {

            //setDescription($newDescription);
            $this->description = $newDescription;
            
            //setName($newName);
            $this->nom = $newName;

            
            $this->answer1 = $newAnswer1;
            $this->answer2 = $newAnswer2;
            $this->answer3 = $newAnswer3;
            $this->answer4 = $newAnswer4;
            $this->answer5 = $newAnswer5;

            $this->idmembre = $newIdMembre;

            $this->ville = $newCity;
            $this->entreprise = $newCompany;

            //setCreationDate($newDate);
            $this->creation_date = new DateTime();
            
            
            $this->end_date = $newEndDate;


            $this->idGroup = $newIdGroup;

            include 'sqlrequest.php';

            try{
                
                $req = $bdd->prepare(
                    'INSERT INTO Paris(nom_paris, idadmin_paris, creation_date, end_date, ville, entreprise, description, answer1, answer2, answer3, answer4, answer5, idgroupe, good_answer) 
                    VALUES(:nom_paris, :idadmin_paris, NOW(),:end_date, :ville, :entreprise, :description, :answer1, :answer2, :answer3, :answer4, :answer5, :idgroupe , :good_answer)'
                    );
            }
            catch(Exception $e){
                die('Erreur : '.$e->getMessage());
            }

            $req->execute(array(
                // 'idparis' => ($idparis),     AUTO INCREMENT
                'nom_paris' => $this->nom,
                'idadmin_paris' => $this->idmembre,     // SESSION ?
                //'creation_date' => $this->creation_date,
                'end_date' => $this->end_date->format('Y-m-d H:i:s'),
                'ville' => $this->ville,
                'entreprise' => $this->entreprise,
                'description' => $this->description,
                'answer1' => $this->answer1,
                'answer2' => $this->answer2,
                'answer3' => $this->answer3,
                'answer4' => $this->answer4,
                'answer5' => $this->answer5,
                'idgroupe' => $this->idGroup,
                'good_answer' => 0
            ));



        }




   
    }
?>