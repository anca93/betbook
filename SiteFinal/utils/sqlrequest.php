<?php 

    include 'utils/connexionBdd.php';


    //rajout reponse dans la bdd
    //**********************************************************************************//
    $addAnswer = $bdd->prepare('INSERT INTO Vote(idparis, idmembre, idreponse_vote)  VALUES ( ?, ?, ?)');
    //**********************************************************************************//

    //selection la réponse selectionnée
    //**********************************************************************************//
    $getAnswer= $bdd->prepare('SELECT DISTINCT Vote.idreponse_vote FROM Vote WHERE Vote.idmembre = ? AND Vote.idparis = ?');
    //**********************************************************************************//

    //selection des votes d'un membre
    //**********************************************************************************//
    $getVote = $bdd->prepare('SELECT * FROM Vote WHERE idmembre = ?');
    //**********************************************************************************//

    //definition bonne reponse
    //**********************************************************************************//
    $addGoodAnswer = $bdd->prepare('UPDATE Paris SET good_answer = ? WHERE idparis = ?');
    //**********************************************************************************//

    //changement score global d'utilisateur
    //**********************************************************************************//
    $updateScore = $bdd->prepare('UPDATE Membre SET score = ? WHERE idmembre = ?');
    //**********************************************************************************//

    //changement score groupe d'utilisateur
    //**********************************************************************************//
    $updateScoreGroupe = $bdd->prepare('UPDATE Jonction SET score_groupe = ? WHERE idmembre = ? AND idgroupe = ?');
    //**********************************************************************************//

    //recherche membres
    //**********************************************************************************//
    $searchUsers = $bdd->prepare("SELECT * FROM Membre WHERE pseudo LIKE :searchTerm ORDER BY pseudo ASC");
    //**********************************************************************************//

    //recherche membres
    //**********************************************************************************//
    $searchGroups = $bdd->prepare("SELECT * FROM Groupe WHERE nom_groupe LIKE :searchTerm ORDER BY nom_groupe ASC");
    //**********************************************************************************//

    //selection photo membre
    //**********************************************************************************//
    $getPicture = $bdd->prepare('SELECT picture FROM Membre WHERE idmembre =?');
    //**********************************************************************************//

    //information du membre a partir du pseudo
    //**********************************************************************************//
    $getMembre = $bdd->prepare('SELECT pseudo, picture, birthday, ville, mail, entreprise, score FROM Membre WHERE pseudo = :pseudo');
    //**********************************************************************************//

    //delete member from group
    //**********************************************************************************//
    $deleteJonction = $bdd->prepare('DELETE FROM Jonction WHERE idmembre = ? AND idgroupe = ?');
    //**********************************************************************************//


    //efface paris el les votes associes
    //**********************************************************************************//
    $deleteBet = $bdd->prepare('DELETE FROM Paris WHERE idparis = ?');
    $deleteVote = $bdd->prepare('DELETE FROM Vote WHERE idparis = ?');
    //**********************************************************************************//


    //selectionne les Identifiants des paris du l'utilisteur connecté
    //**********************************************************************************//
    $nbrBets= $bdd->prepare('SELECT DISTINCT idparis FROM Jonction WHERE idMembre=:idMembre');
    $nbrBets->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);

    //**********************************************************************************//

        //récupère la liste des invitations par idmembre
    //**********************************************************************************//
    $recupinvite = $bdd->prepare('SELECT DISTINCT nom_groupe FROM Groupe WHERE idgroupe IN (SELECT idgroupe_invitation FROM Invitation WHERE idmembre_invitation=:idmembre) LIMIT 5');


    //**********************************************************************************//

    //compte le nombre d'invitation
    //**********************************************************************************//
    $recupnbinvite = $bdd->prepare('SELECT DISTINCT COUNT(nom_groupe) AS count FROM Groupe WHERE idgroupe IN (SELECT idgroupe_invitation FROM Invitation WHERE idmembre_invitation=:idmembre)');
    $recupnbinvite->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);


    //**********************************************************************************//

    //check si l'user a une firm
    //**********************************************************************************//
    $checkentreprise = $bdd->prepare('SELECT entreprise FROM Membre WHERE idmembre=:idmembre');


    //**********************************************************************************//

     //check si l'user a une city
    //**********************************************************************************//
    $checkcity = $bdd->prepare('SELECT ville FROM Membre WHERE idmembre=:idmembre');


    //**********************************************************************************//

    //selectionne lle nom du group auquel appartient le paris
    //**********************************************************************************//
    $getGroupFromBet = $bdd->prepare('SELECT Groupe.nom_groupe, Groupe.idgroupe FROM Groupe, Paris WHERE Groupe.idgroupe = Paris.idgroupe AND Paris.idparis = ?');
    //**********************************************************************************//

    //selectionne les différents nom des groupes de l'utilisateur connecté
    //**********************************************************************************//
    $header = $bdd->prepare('SELECT DISTINCT nom_groupe, Groupe.idgroupe FROM Groupe,Jonction WHERE Jonction.idmembre = :idMembre AND Groupe.idGroupe = Jonction.idgroupe');
    $header->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
    //**********************************************************************************//


    //check que l'utilisateur n'a pas plus de 5 paris créé dans la journée
    //**********************************************************************************//
    $creationmax = $bdd->prepare('SELECT COUNT(idparis) AS max FROM Paris WHERE idadmin_paris=:idmembre AND DATE(creation_date)=DATE(NOW())');

    //**********************************************************************************//

    //selectionne le pseudo qui correspond à un ID membre
    //**********************************************************************************//
    $getNameUser = $bdd->prepare('SELECT DISTINCT pseudo FROM Membre WHERE idmembre = ?');
    //**********************************************************************************//

    //selectionne le score qui correspond à un ID membre
    //**********************************************************************************//
    $getScoreUser = $bdd->prepare('SELECT DISTINCT score FROM Membre WHERE idmembre = ?');
    //**********************************************************************************//

    //selectionne le score_groupe du membre
    //**********************************************************************************//
    $getScoreGroupe = $bdd->prepare('SELECT DISTINCT score_groupe FROM Jonction WHERE idmembre = ? AND idgroupe = ?');
    //**********************************************************************************//
    
    //selection tout les paris en cours d'un membre
    // ne fonctionne plus, changement bdd
     //**********************************************************************************//
        $getAllParis = $bdd->prepare('SELECT * FROM Jonction WHERE idmembre = ?');
     //**********************************************************************************//
    
    //selection les informations d'un paris 
    //**********************************************************************************//
    $getParis = $bdd->prepare('SELECT * FROM Paris WHERE idparis = ?');
    //**********************************************************************************//

    //selection les paris d'un groupe d'un utilisateur
    //**********************************************************************************//
    $bets = $bdd->prepare('SELECT Paris.* FROM Groupe, Paris WHERE Groupe.idgroupe = Paris.idgroupe AND Paris.idadmin_paris = ? AND Groupe.idgroupe = ?');
    //**********************************************************************************//

    //selection des utilisateurs d'un paris
    //**********************************************************************************//
    $getUsers = $bdd->prepare('SELECT DISTINCT Vote.idmembre FROM Jonction, Paris, Vote WHERE Jonction.idgroupe = Paris.idgroupe AND Vote.idparis = Paris.idparis AND Paris.idparis= ?' );

    //**********************************************************************************//

    //selection des utilisateurs d'un paris
    //**********************************************************************************//
    $getUsersVote = $bdd->prepare('SELECT DISTINCT idmembre FROM Vote WHERE idparis= ?' );

    //**********************************************************************************//

    //selectionne le groupe 
    //**********************************************************************************//
    $getGroup= $bdd->prepare('SELECT * FROM Groupe WHERE idgroupe= ?');
    //**********************************************************************************//

    //selectionne des paris pour la page d'acceuil. Cette fonction doit etre modifié (selection random par exemple )
    //**********************************************************************************//
    $randomBets = $bdd->prepare("SELECT DISTINCT Paris.* FROM Paris WHERE idgroupe = :idGroupe LIMIT :betInit , 4");
    $randomBets->bindParam(':betInit', $betInit, PDO::PARAM_INT);
    $randomBets->bindParam(':idGroupe', $_GET['id'], PDO::PARAM_INT);
    //**********************************************************************************//

    //compte le nombre de paris, necessaire pour l'analyse de la variable $_GET['page']
    //**********************************************************************************//
    $randomBetsUnlimited = $bdd->prepare("SELECT DISTINCT * FROM Paris WHERE idgroupe = :idGroupe");
    $randomBetsUnlimited->bindParam(':idGroupe',$_GET['id'], PDO::PARAM_INT);
    //**********************************************************************************//

    //selectionne des paris d'un groupe
    //**********************************************************************************//
    $groupeBets = $bdd->prepare("SELECT DISTINCT * FROM Paris WHERE idgroupe = ?"); 
    //**********************************************************************************//

    //Récupère le nombre max de paris 

    //selectionne des paris d'un groupe avec limite
    //**********************************************************************************//
    $groupeBetsLimit = $bdd->prepare("SELECT DISTINCT * FROM Paris WHERE idgroupe = :idGroupe ORDER BY end_date ASC LIMIT :betInit , 4"); 
    $groupeBetsLimit->bindParam(':betInit', $betInit, PDO::PARAM_INT);
    $groupeBetsLimit->bindParam(':idGroupe', $_GET['id'], PDO::PARAM_INT);
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score
    $recuppseudo = $bdd->prepare('SELECT idmembre, pseudo FROM Membre ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//


     //selectionne des paris d'un groupe avec limite
    //**********************************************************************************//
    $groupeBetsIndex = $bdd->prepare("SELECT DISTINCT * FROM Paris WHERE idgroupe = :idgroupe ORDER BY end_date DESC"); 
    
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur entreprise
    $recuppseudoentreprise = $bdd->prepare('SELECT idmembre, pseudo FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur ville
    $recuppseudoville = $bdd->prepare('SELECT idmembre, pseudo FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur groupe
    $recuppseudogroup = $bdd->prepare('SELECT Membre.idmembre, pseudo FROM Membre, Jonction WHERE idgroupe=:idgroupe AND Jonction.idmembre=Membre.idmembre ORDER BY score_groupe DESC LIMIT 30');
    //**********************************************************************************//

    //Récupère le score
    $recupscore = $bdd->prepare('SELECT idmembre, score FROM Membre ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//


    //Récupère le score par entreprise
    $recupscoreentreprise = $bdd->prepare('SELECT idmembre, score FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Récupère le score par entreprise
    $recupscoreville = $bdd->prepare('SELECT idmembre, score FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Récupère le score par entreprise
    $recupscoregroup = $bdd -> prepare ('SELECT idmembre, score_groupe FROM Jonction WHERE idgroupe=:idgroupe ORDER BY score_groupe DESC LIMIT 30');
    //**********************************************************************************//

    $getIdGroupe = $bdd-> prepare('SELECT DISTINCT idmembre FROM Jonction WHERE idgroupe = ?');

    //Récupère les id des membres d'un groupe
    $getIdGroupe4 = $bdd-> prepare('SELECT DISTINCT idmembre FROM Jonction WHERE idgroupe =:idGroupe  LIMIT :profilInit,4');
    $getIdGroupe4->bindParam(':profilInit', $profilInit , PDO::PARAM_INT);
    $getIdGroupe4->bindParam(':idGroupe', $_GET['id'], PDO::PARAM_INT);
    //**********************************************************************************//


    //Recupère le nb max de personnes, sans dépasser 30
    $recupclass = $bdd->prepare('SELECT COUNT(*) AS class FROM Membre  LIMIT 30');
    //**********************************************************************************//

    //Recupère le nb max de personnes par entreprise, sans dépasser 30
    $recupclassentreprise = $bdd->prepare('SELECT COUNT(*) AS class FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) LIMIT 30');
    //**********************************************************************************//

    //Recupère le nb max de personnes par ville, sans dépasser 30
    $recupclassville = $bdd->prepare('SELECT COUNT(*) AS class FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) LIMIT 30');
    //**********************************************************************************//

    //Recupère le nb max de personnes dans un groupe
    $recupclassgroup = $bdd->prepare('SELECT COUNT(*) AS class FROM Jonction WHERE idgroupe=:idgroupe LIMIT 30');
    //**********************************************************************************//

    //Membre
     $getUsersData = $bdd->prepare('SELECT * FROM Membre WHERE idmembre=? ');
    //**********************************************************************************//





    //Recupère l'id des membres', sans dépasser 30
    $recupid = $bdd->prepare('SELECT idmembre FROM Membre ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère l'id des membres dans un groupe, sans dépasser 30
    $recupidgroup = $bdd->prepare('SELECT idmembre FROM Jonction WHERE idgroupe=:idgroupe ORDER BY score_groupe DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère l'id des membres par entreprise, sans dépasser 30
    $recupidentreprise = $bdd->prepare('SELECT idmembre FROM Membre WHERE entreprise IN (SELECT entreprise FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //Recupère les pseudo des joueurs en fonction de leur score et de leur ville
    $recupidville = $bdd->prepare('SELECT idmembre FROM Membre WHERE ville IN (SELECT ville FROM Membre WHERE idmembre=:idmembre) ORDER BY score DESC LIMIT 30');
    //**********************************************************************************//

    //récupère le nom des groupes d'un user
    $recupgroup = $bdd->prepare('SELECT nom_groupe, idgroupe FROM Groupe WHERE idgroupe IN (SELECT idgroupe FROM Jonction WHERE idmembre=:idmembre) GROUP BY idgroupe');
    //**********************************************************************************//

    //Recupère le pseudo du user connecté
    $recuppseudouser = $bdd->prepare('SELECT pseudo FROM Membre WHERE idmembre=:idmembre');
    //**********************************************************************************//

    //Recupère le score du user connecté
    $recupscoreuser = $bdd->prepare('SELECT score FROM Membre WHERE idmembre=:idmembre');
    //**********************************************************************************//

    //Recupère le score du user connecté dans un groupe
    $recupscoregroupeuser = $bdd->prepare('SELECT score FROM Jonction WHERE idmembre=:idmembre AND idgroupe=:idgroupe');
    //**********************************************************************************//

    //récupère le nom d'un user
    $recupname = $bdd->prepare('SELECT pseudo FROM Membre WHERE idmembre=:idmembre');
    //**********************************************************************************//

?>