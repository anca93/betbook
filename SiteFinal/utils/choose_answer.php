<div class="col-lg-12 col-md-12">
				        		<div class="row">
				        			<div class="col-md-12  col-xs-12">
							    		<div class="col-md-12  col-xs-12 bottom_bet shadow">
								    		<div class="row">
								    			<div class="col-md-12 col-xs-12 bg-primary center_text">
										    		<p >
										    			<h4>Your answer</h4>
												    	</p>
										    	</div>

										    	<div class="btn-group-vertical btn-block no_radius">
										    		<?php 
									    			if($donneesParis['answer1'] != 'NONE')
									    			{ 
									    				?>
										    			<form action="postAnswer.php" method="POST">
										    			<input type="hidden" value="1" name="answer">
										    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
													    <button type"submit" class="btn btn-info btn-lg no_radius btn-block" >	  
													    	<div class="raw">
														    	<div class="col-md-10 col-xs-10 left-text">
														    		<?php echo $donneesParis['answer1']?>
														    	</div>
														    	<div class="col-md-2 col-xs-2">
														    	<?php
														    		if( $donneesAnswer['idreponse_vote'] == 1)
														    		{
														    	?>
														    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
														    	<?php
														    		}
														    		else
														    		{
														    	?>
														    		<i class="fa fa-square-o" aria-hidden="true"></i>	
														    	<?php
														    		}
														    	?>
													    	</div>
													    </button>
													    </form>
													<?php
													}
										    		?>

										    		<?php 
									    			if($donneesParis['answer2'] != 'NONE')
									    			{ 
									    				?>
										    			<form action="postAnswer.php" method="POST">
										    			<input type="hidden" value="2" name="answer">
										    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
													    <button type"submit" class="btn btn-success btn-lg no_radius btn-block" >
													    	 <div class="raw">
														    	<div class="col-md-10 col-xs-10 left-text">
														    		<?php echo $donneesParis['answer2']?>
														    	</div>
														    	<div class="col-md-2 col-xs-2">
														    	<?php
														    		if( $donneesAnswer['idreponse_vote'] == 2)
														    		{
														    	?>
														    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
														    	<?php
														    		}
														    		else
														    		{
														    	?>
														    		<i class="fa fa-square-o" aria-hidden="true"></i>	
														    	<?php
														    		}
														    	?>
														    	</div>
													    	</div>
													    </button>
													    </form>
													<?php
													}
										    		?>

										    		<?php 
									    			if($donneesParis['answer3'] != 'NONE')
									    			{ 
									    				?>
										    			<form action="postAnswer.php" method="POST">
										    			<input type="hidden" value="3" name="answer">
										    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
													    <button type"submit" class="btn btn-warning btn-lg no_radius btn-block" >
													    	 <div class="raw">
														    	<div class="col-md-10 col-xs-10 left-text">
														    		<?php echo $donneesParis['answer3']?>
														    	</div>
														    	<div class="col-md-2 col-xs-2">
														    	<?php
														    		if( $donneesAnswer['idreponse_vote'] == 3)
														    		{
														    	?>
														    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
														    	<?php
														    		}
														    		else
														    		{
														    	?>
														    		<i class="fa fa-square-o" aria-hidden="true"></i>	
														    	<?php
														    		}
														    	?>
													    	</div>
													    </button>
													    </form>
													<?php
													}
										    		?>
										    		<?php 
									    			if($donneesParis['answer4'] != 'NONE')
									    			{ 
									    				?>
										    			<form action="postAnswer.php" method="POST">
										    			<input type="hidden" value="4" name="answer">
										    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
													    <button type"submit" class="btn btn-danger btn-lg no_radius btn-block" >
													    	 <div class="raw">
														    	<div class="col-md-10 col-xs-10 left-text">
														    		<?php echo $donneesParis['answer4']?>
														    	</div>
														    	<div class="col-md-2 col-xs-2">
														    	<?php
														    		if( $donneesAnswer['idreponse_vote'] == 4)
														    		{
														    	?>
														    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
														    	<?php
														    		}
														    		else
														    		{
														    	?>
														    		<i class="fa fa-square-o" aria-hidden="true"></i>	
														    	<?php
														    		}
														    	?>
													    	</div>
													    </button>
													    </form>
													<?php
													}
										    		?>
										    		<?php 

									    			if($donneesParis['answer5'] != 'NONE')
									    			{ 
									    				?>
										    			<form action="postAnswer.php" method="POST">
										    			<input type="hidden" value="5" name="answer">
										    			<input type="hidden" value="<?php echo $_GET['id']?>" name="idParis">
													    <button type"submit" class="btn btn-primary btn-lg no_radius btn-block" >
													    	 <div class="raw">
														    	<div class="col-md-10 col-xs-10 left-text">
														    		<?php echo $donneesParis['answer5']?>
														    	</div>
														    	<div class="col-md-2 col-xs-2">
														    	<?php
														    		if( $donneesAnswer['idreponse_vote'] == 5)
														    		{
														    	?>
														    		<i class="fa fa-check-square-o" aria-hidden="true"></i>
														    	<?php
														    		}
														    		else
														    		{
														    	?>
														    		<i class="fa fa-square-o" aria-hidden="true"></i>	
														    	<?php
														    		}
														    	?>
													    	</div>
													    </button>
													    </form>
													<?php
													}
										    		?>

										    	</div>
										    </div>
									    </div>	
					        		</div>
					        	</div>
					        </div>