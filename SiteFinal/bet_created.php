<?php
 

    //variable à initialiser avec les variable de scession
    //**********************************************************************************//
    $idmembre =  $_SESSION['idmembre'];
    //**********************************************************************************//

    //include des request sql et connection bdd
    //**********************************************************************************//
    include 'utils/sqlrequest.php';
    //**********************************************************************************//

    //**********************************************************************************//


    include_once("utils/Pari.class.php");

    $ans3="NONE";
    $ans4="NONE";
    $ans5="NONE";
    $ci="NONE";
    $fi="NONE";

    if($_POST["answer3"]!=""){
        $ans3 = htmlspecialchars($_POST["answer3"]);
    }
    if($_POST["answer4"]!=""){
        $ans4 = htmlspecialchars($_POST["answer4"]);
    }
    if($_POST["answer5"]!=""){
        $ans5 = htmlspecialchars($_POST["answer5"]);
    }

    if( (isset($_POST["city_check"]) && ($_POST["city"]!=""))) {
        $ci = htmlspecialchars($_POST["city"]);
    }

    if( (isset($_POST["firm_check"]) && $_POST["firm"]!="")){
        $fi = htmlspecialchars($_POST["firm"]);
    }

    if( $_POST["hour"]!="" && $_POST["day"]!=""){
        $end_ex = htmlspecialchars($_POST["day"]).' '.htmlspecialchars($_POST["hour"].':00');
        $end = date_create_from_format('d/m/Y H:i:s',$end_ex);

    }

    
    $pari = new Pari(
        htmlspecialchars($_POST["details"]),
        htmlspecialchars($_POST["question"]),
        htmlspecialchars($_POST["answer1"]),
        htmlspecialchars($_POST["answer2"]),
        $ans3,
        $ans4,
        $ans5,
        $ci,
        $fi,
        $end,
        $idmembre,
        htmlspecialchars($_POST["idGroup"])
        );

    header('Location: groupe.php?id='.$_POST["idGroup"]);

    exit();

/*

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <!-- perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">
    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!-- Bootstrap Vertical Nav -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-vertical-menu.css">

</head>
<body>

 <?php include("utils/navbar.php"); ?>

<div class="container">
    <div class="row">
         <?php include("utils/leftmenu.php"); ?>

         <div class="col-md-9 col-md-9 top container">

            <p>
    your question:
    <br/>
    <strong>
    <?php
    echo $pari->getName();
    ?>
    </strong>
    

    <?php

        if ($pari->getCompany() != NULL) {  //devient inutile avec 'NONE'
            ?>

            <br/><br/> company tagged: <br/> <strong>
            <?php
            echo $pari->getCompany();
        }

    ?> </strong> <?php

    if ($pari->getCity() != NULL) {         //devient inutile avec 'NONE'
            ?>

            <br/><br/> city tagged: <br/> <strong>
            <?php
            echo $pari->getCity();
        }

    ?> </strong>

    <br/><br/>
    details about it:
    <br/>

    <strong>

    <?php
    echo $pari->getDescription();
    ?>
    </strong>
    <br/><br/>
    possible answers given:
    <strong>
    <ul>
        
    <?php
    
    
        foreach ($pari->getAnswers() as $value) {
            echo "<li>".$value."</li>";
        }

    ?>
    </strong>
    </ul>
    <br/><br/>

    id author (admin):
    <br/><strong>

    <?php
    echo $pari->getIdMembre();
    ?>
    </strong>
    <br/><br/>


    creation date:
    <strong>

    <?php
    echo $pari->getCreationDate()->format('d-m-Y H:i:s');
    ?>
    </strong>
    <br/><br/>

    end of the bet date: 
    <strong>

    <?php
    echo $pari->getEndDate()->format('d-m-Y H:i:s');
    ?>
    </strong>
    <br/><br/>



     <a href="bet_form.php" >cancel (pd)</a>

</p>
        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</body>
</html>

*/

?>