<?php
	
    session_start();

	//IL FAUT MODIFIER LA CONNECTION À LA BASE DE DONNÉE

	//connection à la BDD
	try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'root', 'busi');
    }
    catch(Exception $e)
    {
            die('Erreur : '.$e->getMessage());
    } 

    //variable à initialiser avec les variable de scession
    $idMembre = $_SESSION['idmembre'];
	$nbrParis = 0;
	
    //selectionne les Identifiants des paris du l'utilisteur connecté
  	//**********************************************************************************//
    $nbrBets= $bdd->prepare('SELECT DISTINCT idparis FROM Jonction WHERE idMembre=:idMembre');
    $nbrBets->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
    //**********************************************************************************//

    //selectionne les différents nom des groupes de l'utilisateur connecté
  	//**********************************************************************************//
    $header = $bdd->prepare('SELECT DISTINCT nom_groupe FROM Groupe,Jonction WHERE Link.idmembre = :idMembre AND Groupe.idMembre = Link.idgroupe');
    $header->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
  	//**********************************************************************************//
   
   	//selection les paris d'un groupe d'un utilisateur
  	//**********************************************************************************//
    $bets = $bdd->prepare('SELECT DISTINCT nom_paris FROM Paris, Jonction, Groupe WHERE Link.idparis=Paris.idparis AND Link.idgroupe = Groupe.idgroupe AND link.idmembre = ?  AND Groupe.nom_groupe = ?' );
  	//**********************************************************************************//

    //calcule le nombre de paris de l'utilisateur connecté
  	//**********************************************************************************//
    $nbrBets->execute();
    $nbrParis = 0;
    while($donnees = $nbrBets->fetch())
    {
    	$nbrParis += 1;
    }
   	$nbrBets->closeCursor();  	
   	//**********************************************************************************//
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <!-- Bootstap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    
    <!-- CSS perso -->
    <link rel="stylesheet" href="bootstrap/css/perso.css">

    <!-- pour les icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-3">

            <div class="bootstrap-vertical-nav">

                <nav class="navbar navbar-light bg-faded">
                    <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
                        &#9776;
                    </button>
                    <div class="collapse navbar-toggleable-sm" id="exCollapsingNavbar2">
                        
                        <a class="navbar-brand" href="#">Welcome to BetBook</a>
                        
                        <ul class="nav navbar-nav">

                             <li class="nav-item active">
                                <a class="nav-link" href="http://localhost/projet_web/profil/profil.php">                               									<i class="fa fa-user fa-fw bg2 ellipsis"></i>
                                	<?php
                                    echo $_SESSION['pseudo'];
                                    ?>
                                </a>
                            </li>   

                            <li class="nav-item active">
                                <a class="nav-link  ellipsis titleGroups" href="#">                               									<i class="fa fa-users fa-fw bg2 ellipsis"></i>
                                	My groups
                                </a>
                            </li>

                            <?php
							    $header->execute();
							    while($donneesHeader = $header->fetch())
							    {
							?>
										    	
	                             <li class="nav-item active">
	                                <a class="nav-link text-primary ellipsis	" href="#">#<?php echo $donneesHeader['nom_groupe']?><span class="sr-only">(current)</span></a>
	                            </li>
	                         <?php
	                         	}
	                         ?>

							<li class="nav-item active">
								<div class="dropdown">
									<button class="btn btn-primary btn-block dropdown-toggle" type="button" data-toggle="dropdown">My bets
									<span class="bagde"> (<?php echo $nbrParis?>) </span></button>
									<ul class="dropdown-menu">

										<?php
										    $header->execute();
										    while($donneesHeader = $header->fetch())
										    {
										    ?>
										    	<li class="dropdown-header"><?php echo $donneesHeader['nom_groupe']?></li>
										    <?php
											    $bets->execute(array($idMembre, $donneesHeader['nom_groupe']));
											    while($donneesBets = $bets->fetch())
											    {
											    	?>
											    	<li class="dropdown_menu">
											    		<a href="#">
												    		<i> <?php echo $donneesBets['nom_paris']?></i>
												    	</a>
											    	</li>
											    	<?php
											    }
											    $bets->closeCursor();
										    }
										   	$header->closeCursor();
										?>
									</ul>
								</div>
							</li>

							<li class="nav-item active">
                                <a class="nav-link ellipsis" href="#">                               									<i class="fa fa-cogs fa-fw bg2 ellipsis"></i>
                                	Settings
                                </a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="col-md-9 top">
        	<div class="row">
        	<form class="navbar-form navbar-left" role="search">
				<div class="col-md-9 col-xs-8">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search BetBook">
					</div>
				</div>
			<div class="col-md-3 col-xs-4">
				<form method="post" action="#">
					<button type="submit" class="btn btn-primary btn-block shadow"><i class="fa fa-eye fa-1x" aria-hidden="true"></i> Submit</button>
				</form>
			</div>
			</form>
	       		<div class="col-md-12 col-xs-12 bg3">
	       			<div class="row">

	        			<h1 class="titre">Ici tu mets ton titre</h1>
	        		
	        		<div class="row ">
	        			<div class="col-md-12 col-xs-12">

	        			ici tu mets ta page
	        	</div>
        	</div>
        	
       		<div class="col-md-12 col-xs-12">
	        	<button type="button" class="btn btn-primary btn-lg btn-block shadow">
	        		Try to post a Bet !
	        	</button>
        	</div>
        </div>
    </div>
</div>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
</body>
</html>
