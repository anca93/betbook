<?php

	class Groupe
	{
		private $idgroupe;
		private $nom_groupe;
		private $creation_date_groupe;
		private $idadmin_groupe;
		private $description_groupe;

		public function getIdGroupe()
		{
			return $this->idgroupe;
		}

		public function getName()
		{
			return $this->nom_groupe;
		}

		public function getCreationDate()
		{
			return $this->creation_date_groupe;
		}

		public function getIdAdmin()
		{
			return $this->idadmin_groupe;
		}

		public function getDescription()
		{
			return $this->description_groupe;
		}

		public function setIdGroupe($newID)
		{
			$this->idgroupe = $newID;
		}

		public function setName($newName)
		{
			$this->nom_groupe = $newName;
		}

		public function setCreationDate($newDate)
		{
			$this->creation_date_groupe = $newDate;
		}

		public function setIdAdmin($newID)
		{
			$this->idadmin_groupe = $newID;
		}

		public function setDescription($newDescription)
		{
			if (!empty($newDescription) ){
                $this->description_groupe = $newDescription;
            }
		}

		public function insertionBDD()
        {

            $bdd= new PDO('mysql:host=localhost;dbname=test','root','');

            $req = $bdd->prepare('INSERT INTO Groupe(idgroupe, nom_groupe, creation_date_groupe, idadmin_groupe, description_groupe) VALUES(:idgroupe, :nom_groupe, :creation_date_groupe, :idadmin_groupe, :description_groupe)');
            $req->execute(array(
                `idgroupe` => $idgroupe,
  				`nom_groupe` => $nom_groupe,
  				`creation_date_groupe` => $creation_date_groupe,
  				`idadmin_groupe` => $idadmin_groupe,
  				`description_groupe` => $description_groupe
            ));


        }  


	}
?>