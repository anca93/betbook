<?php
    //database configuration
    $dbHost = 'localhost';
    $dbUsername = 'root';
    $dbPassword = '';
    $dbName = 'BetBook';
    
    //connect with the database
    $db = new mysqli($dbHost,$dbUsername,$dbPassword,$dbName);
    
    //get search term
    $searchTerm = $_GET['term'];
    
    //get matched data from skills table
    $query = $db->query("SELECT * FROM Membre WHERE pseudo LIKE '%".$searchTerm."%' ORDER BY pseudo ASC");
    while ($row = $query->fetch_assoc()) {
        $data[] = $row['pseudo'];
    }
    
    //return json data
    echo json_encode($data);
?>