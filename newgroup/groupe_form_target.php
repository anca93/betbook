<H1> Hello group creator ! </H1>


<?php
/*
    session_start();
    $pseudo = $_SESSION['pseudo'];
    $idadmin_groupe = $_SESSION['idmembre'];
    */

$idadmin_groupe = 1;

$description_groupe = htmlspecialchars($_POST["details"]);
$nom_groupe = htmlspecialchars($_POST["name"]);

$friends_string = htmlspecialchars($_POST["friend"]);
$friends_array = preg_split("/[\s;]+/", $friends_string);


try
{
	$bdd = new PDO('mysql:host=localhost;dbname=BetBook;charset=utf8','root','',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

	$req = $bdd->prepare('INSERT INTO Groupe(nom_groupe, creation_date_groupe, idadmin_groupe, description_groupe) VALUES(:nom_groupe, CURDATE(), :idadmin_groupe, :description_groupe)');

	$req->execute(array('nom_groupe' => $nom_groupe , 'idadmin_groupe' => $idadmin_groupe, 'description_groupe' => $description_groupe));

	$idgroupe = $bdd->lastInsertId();
	
	$req = $bdd->prepare('INSERT INTO Jonction(idmembre, idgroupe, idparis, score_groupe) VALUES(:idmembre, :idgroupe, :idparis, :score_groupe)');

	$req->execute(array('idmembre' => $idadmin_groupe, 'idgroupe' => $idgroupe, 'idparis' => 0, 'score_groupe' => 0));

	foreach ($friends_array as $value) {
		if($value != null)
		{
			$req = $bdd->prepare("SELECT idmembre FROM Membre WHERE pseudo = :pseudo");

			$req->execute(array(':pseudo' => $value));

			$idmembre_row = $req->fetch(PDO::FETCH_ASSOC);

			$idmembre = $idmembre_row['idmembre'];

			if($idmembre != 0)
			{

				$req = $bdd->prepare('INSERT INTO Jonction(idmembre, idgroupe, idparis, score_groupe) VALUES(:idmembre, :idgroupe, :idparis, :score_groupe)');

				$req->execute(array('idmembre' => $idmembre, 'idgroupe' => $idgroupe, 'idparis' => 0, 'score_groupe' => 0));
			}
		}
	}
	

	
} 
catch(Exception $e)
{	
	die('Erreur : '.$e->getMessage());
}

?>

	<p>
		your group:
		<br/>
		<strong>
			<?php

			echo $idgroupe;
			?>
		</strong>
		<br/><br/>
		details about it:
		<br/>
		<strong>

			<?php

			echo $description_groupe;
			?>

			<br/>
			<br/>
			<a href="groupe_form.php" >cancel (pd)</a>

		</p>