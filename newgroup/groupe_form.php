<!DOCTYPE html>
<html>

<?php
/*
    session_start();
    $pseudo = $_SESSION['pseudo'];
    $id_admin = $_SESSION['idmembre'];
    */

    $errors = array();

    if ( isset($_POST["submit"]) ) {

        if ( strlen(   $_POST["name"]   ) < 5){
            $errors["name"][] = "Non valid name (less than 5 characters)." ;
        }

        if (count($errors) == 0) {
            // Pas d'erreurs de saisies
            include("groupe_form_target.php");
            die();
        }
    }
?>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="/BetBook/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="groupe_form.css" />    
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <title>BetBook : New Groupe</title>

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <!-- Autocomplete Multiple Values -->
        <script>
        $(function() {
            function split( val ) {
                return val.split( /;\s*/ );
            }
            function extractLast( term ) {
                return split( term ).pop();
            }
            
            $( "#keyword" ).bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                minLength: 1,
                source: function( request, response ) {
                    // delegate back to autocomplete, but extract the last term
                    $.getJSON("autocomplete.php", { term : extractLast( request.term )},response);
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "; " );
                    return false;
                }
            });
        });
        </script>


    </head>

    <body>
        <div class="container">
        <h2><strong> New Group </strong></h2>
        <form method="post">

            <i>Please fill out this form to create the group.</i>
        </br>
        </br>

        <?php
        // Si des erreurs ont été trouvées, les afficher sous forme de liste
            if (count($errors) > 0) {
                echo "<div class=\"error\">";
                echo "<ul>";
                foreach ($errors as $champEnErreur => $erreursDuChamp) {
                    foreach ($erreursDuChamp as $erreur) {
                        echo "<li>".$erreur."</li>";
                    }
                }
                echo "</ul>";
                echo "</div>";
            }
        ?>

        </br>
        
            
            
        <div class="row">

                <div class="col-sm-12">

                    Name
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" />
                    </div>

                    Description
                    <div class="form-group">
                        <textarea name="details" rows="2" class="form-control"></textarea>
                    </div>

                    Add friends

                    <div class="ui-widget">
                        <input name="friend" id="keyword" placeholder="Search friends" type="text" class="form-control"/>

                    </div> 

                    </br></br>

                </div>
               
            
                    <strong></br><h4><center>
                    Ready to rule the world ?
                    </br>
                    <button type="submit" name="submit" class="btn btn-primary btn-lg"> Submit </button>
                    </strong></h4></center>
                </br>               
    </body>
</html>